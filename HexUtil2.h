//
//  HexUtil.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 6/08/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__HexUtil2__
#define __EasyIntegrator__HexUtil2__

#include <string>
#include "EasyCTEPCpp.h"

//! @cond

  class HexUtil2
  {
  public:
    static std::vector<byte> parse(std::string hex);

  };


//! @endcond

#endif /* defined(__EasyIntegrator__HexUtil2__) */
