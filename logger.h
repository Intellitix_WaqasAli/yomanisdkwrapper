#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QPlainTextEdit>
#include <QFile>
#include <QTextStream>
#include <QDateTime>

class Logger
{

public:
 explicit Logger(QString fileName);
 ~Logger();
 void setShowDateTime(bool value);

private:
 QFile *file;
 bool m_showDate;

public:
 void write(const QString &value);

};

#endif // LOGGER_H
