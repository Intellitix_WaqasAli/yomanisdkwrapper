#-------------------------------------------------
#
# Project created by QtCreator 2018-09-21T17:57:24
#
#-------------------------------------------------

QT       += widgets core

QT       -= gui

greaterThan(QT_MAJOR_VERSION, 4):

TARGET = yomaniSdkWrapper
TEMPLATE = lib

CONFIG += c++14

unix: {
    QMAKE_CXXFLAGS += -Wno-unknown-pragmas -Wall -Wno-unused-local-typedef
}

DEFINES += YOMANISDKWRAPPER_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        yomanisdkwrapper.cpp \
    HexUtil2.cpp \
    logger.cpp

HEADERS += \
        yomanisdkwrapper.h \
    yomanisdkwrapperInterface.h \
    HexUtil2.h \
    logger.h

INCLUDEPATH += $$PWD/ \
               $$PWD/include

DEPENDPATH += $$PWD/

unix {
    target.path = /usr/lib
    INSTALLS += target
}

macx {
       INCLUDEPATH += /usr/local/include
       LIBS += /usr/local/lib/libboost_system.a
       LIBS += $$PWD/../../../../build-ectcpp/out/libeasyctepcpp.dylib
}
unix:!macx {
       LIBS += $$PWD/../../../../build-ectcpp/out/libeasyctepcpp.so
}
win32 {
       INCLUDEPATH += c:/boost_1_59_0
       LIBS += -LC:/boost_1_59_0/lib32-msvc-14.0
       LIBS += -L$$PWD/libs/win_x86
       LIBS += easyctep.lib
       LIBS += easyctepcpp.lib
       LIBS += -lws2_32 -lwsock32
}


target.path = $$PWD/../yomaniSDKwrapperRelease
INSTALLS += target
