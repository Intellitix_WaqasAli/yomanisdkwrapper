//
//  HexUtil.cpp
//  EasyIntegrator
//
//  Created by Marc Jordant on 6/08/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#include "HexUtil2.h"
#include <cstring>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

static uint8_t HEX_BYTES[] = {
    // 0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
    99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99
    /* 0 */, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99
    /* 1 */, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99
    /* 2 */,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 99, 99, 99, 99, 99, 99
    /* 3 */, 99, 10, 11, 12, 13, 14, 15, 99, 99, 99, 99, 99, 99, 99, 99, 99
    /* 4 */, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99
    /* 5 */, 99, 10, 11, 12, 13, 14, 15, 99, 99, 99, 99, 99, 99, 99, 99, 99
    /* 6 */, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99
};

static uint8_t HEX_BYTES_LEN = 128;
static uint8_t HEX_BYTE_SKIP = 99;


std::vector<byte> HexUtil2::parse(std::string hex)
{
    char const *text = hex.c_str();
    size_t len = strlen(text);

    uint8_t high = 0;
    bool highPassed = false;

    std::vector<byte> data;

    for(int i = 0; i < (int)len; i++)
    {
        char index = text[i];

        // checks if value out of 127 (ASCII must contains from 0 to 127)
        if(index >= HEX_BYTES_LEN ) {
            continue;
        }

        uint8_t nibble = HEX_BYTES[(int)index];

        // checks if not HEX chars
        if(nibble == HEX_BYTE_SKIP) {
            continue;
        }

        if(highPassed) {
            // fills right nibble, creates byte and adds it
            uint8_t low = (uint8_t) (nibble & 0x7f);
            highPassed = false;
            uint8_t currentByte = ((high << 4) + low);
            data.push_back(currentByte);
        }

        else {
            // fills left nibble
            high = (uint8_t) (nibble & 0x7f);
            highPassed = true;
        }
    }

    if(highPassed) {
        return std::vector<byte>();
    }

    return data;
}

