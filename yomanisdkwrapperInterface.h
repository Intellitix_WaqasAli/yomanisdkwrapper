#ifndef YOMANISDKWRAPPER_GLOBAL_H
#define YOMANISDKWRAPPER_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QObject>

#if defined(YOMANISDKWRAPPER_LIBRARY)
#  define YOMANISDKWRAPPERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define YOMANISDKWRAPPERSHARED_EXPORT Q_DECL_IMPORT
#endif

enum class WrapperInfo{
    error=1,
    info,
    terminalConnect,
    terminalDisconnect,
    serviceStart,
    serviceStop,
    transactionSuccess,
    transactionPending,
    transactionError
};


#endif // YOMANISDKWRAPPER_GLOBAL_H
