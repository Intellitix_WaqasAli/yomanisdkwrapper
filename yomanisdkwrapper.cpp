#include "yomanisdkwrapper.h"
#include "yomanisdkwrapperInterface.h"

#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>

#include <QDebug>
#include <QThread>

#include <chrono>
#include <thread>

#include "HexUtil2.h"

using namespace std;
using namespace ect;
using namespace std::placeholders;

QT_BEGIN_NAMESPACE

SaleInfo::SaleInfo(): _amount("0")
{
    _saleAction = ACTION_INDEX_CARD_INFO;
    _cardBrand = ACTION_INDEX_CARD_VALIDITY_CHECK;
    _merchRef = "";
}


namespace {
std::string toString(AuthorizedSaleAction saleType)
{
    switch (saleType) {
    case AuthorizedSaleAction::CancelSale:
        return "CancelSale";
    case AuthorizedSaleAction::CancelCashAdvance:
        return "CancelCashAdvance";
    case AuthorizedSaleAction::CashAdvanceAfterVoiceReferral:
        return "CashAdvanceAfterVioceReferral";
    case AuthorizedSaleAction::CancelReservation:
        return "CancelReservation";
    case AuthorizedSaleAction::CancelSaleAfterReservation:
        return "CancelSaleAfterReservation";
    case AuthorizedSaleAction::SaleAfterVoiceReferral:
        return "SaleAfterVoiceReferral";
    case AuthorizedSaleAction::SaleAfterReservation:
        return "SaleAfterReservation";
    default:
        return "Unknown";
    };
    return "Unknown";
}

std::string toString(SaleAction saleType)
{
    switch (saleType) {
    case SaleAction::Sale:
        return "Sale";
    case SaleAction::SaleWithDeliveryConfirmation:
        return "SaleWithDeliveryConfirmation";
    case SaleAction::CashAdvance:
        return "CashAdvance";
    case SaleAction::Reservation:
        return "Reservation";

    case SaleAction::Refund:
        return "Refund";
    default:
        return "Unknown";
    };
    return "Unknown";
}
}

YomaniSdkWrapper::YomaniSdkWrapper(): QObject(), amnt("0"), serviceCreated(false), terminalConnectionStatus(false),/*callbackFunc(nullptr),*/ _saleSettings()
{
    qRegisterMetaType<std::string>("std::string");
    qRegisterMetaType<callback>("callback");
    qRegisterMetaType<StringCallback>("StringCallback");

    m_log.write("Creating connections\n");
//    connect(this, SIGNAL(signalDebugLogOnMainThread(std::string)), SLOT(debugLogOnMainThread(std::string)));
    connect(this, SIGNAL(signalSetConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal>)),this, SLOT(setConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal>)));
    connect(this, SIGNAL(signalRemoveConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal>)),this, SLOT(removeConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal>)));
    connect(this, SIGNAL(signalRemoveAllConnectedTerminalsIdOnMainThread()),this , SLOT(removeAllConnectedTerminalsIdOnMainThread()));
    connect(this, SIGNAL(signalCallBlockOnMainThread(callback)),this , SLOT(callBlockOnMainThread(callback)));

    m_log.write("Connections created\n");
    cardRecoTimeout = "45";
    pendingSaleTimeout = "30";

    isListening = false;

    m_log.write("Calling getLibraryVersion function\n");
    std::string libVersion =  ect::getLibraryVersion();
    QString lV = QString::fromStdString(libVersion);
    m_log.write("Libversion Returned value "+ lV );

}

YomaniSdkWrapper::~YomaniSdkWrapper()
{

}

void YomaniSdkWrapper::refreshSettings()
{
    ectSettings_ = ectService_->settings();

}

/////////////////////////////
// Connection management
/////////////////////////////

void YomaniSdkWrapper::onServiceStart()
{
        isListening = true;
        serviceCreated = true;

        debugLog("Service created and started.");

        refreshSettings();

        ectSettings_ = ectService_->settings();
        ectSettings_.certificationModeEnabled = true;
        ectSettings_.certificationFilePath = ".";
        ectSettings_.certificationFileName = "easying-log.txt";
        setSettings();
        m_log.write("serviceCallback function check");
        if(serviceCallback!=nullptr){
          serviceCallback(static_cast<int>(WrapperInfo::serviceStart),"Worldline service started successfully." );
        }

        debugLog("Waiting for terminal connection...");
}

void YomaniSdkWrapper::onServiceStop()
{
        isListening = false;
        serviceCreated = false;
        debugLog("Service stopped.");
        if(serviceCallback!=nullptr){
            serviceCallback(static_cast<int>(WrapperInfo::serviceStop),"Service stopped");
        }
}

void YomaniSdkWrapper::onServiceError(const ECTError& error)
{
    if(serviceCallback!=nullptr){
        ECTErrorCode errorCode = error.code();
        QString eCode = QString::number(static_cast<int> (errorCode));
        QString errorInfo = QString("Start of the service failed: ");
        errorInfo+= QString::fromStdString(error.description());
        errorInfo += QString(" \nError code : ") +  eCode ;
        debugLog("Recieved Error.");
        debugLog(errorInfo.toStdString());
        if(serviceCallback!=nullptr){
        serviceCallback(static_cast<int>(WrapperInfo::error),errorInfo.toStdString().c_str());
        }
    }

}

void YomaniSdkWrapper::onTerminalConnect(std::shared_ptr<CTEPTerminal> terminal)
{
        auto terminalId = terminal->terminalId();
        terminalConnectionStatus = true;
        debugLog("Terminal connected: " + terminalId);
        setConnectedTerminalIdOnMainThread(terminal);
        terminal->enableNotifications(true);

        if(serviceCallback!=nullptr){

            serviceCallback(static_cast<int>(WrapperInfo::terminalConnect),terminalID.c_str());
        }
}

void YomaniSdkWrapper::onTerminalDisconnect(std::shared_ptr<CTEPTerminal> terminal)
{
        removeConnectedTerminalId(terminal);
        terminalConnectionStatus = false;
        debugLog("Terminal disconnected:" + terminal->terminalId());
        if(serviceCallback!=nullptr){

            serviceCallback(static_cast<int>(WrapperInfo::terminalDisconnect),terminalID.c_str());
        }
}

void YomaniSdkWrapper::onTerminalEvent(const ECTTerminalEvent terminalEvents)
{
        debugLog("Terminal ID: " + terminalEvents.terminalIdentifier + ", event:" + terminalEvents.eventsString());

        // Example of a check on a specific event type: SwipeCard:
        bool isSwipeCard = terminalEvents.isEvent(ECTTerminalEvents::ECTTerminalEventsSwipeCard);
        std::string isSwipeCardStr = isSwipeCard ? "YES" : "NO";
        debugLog("\tis Swipe Card:" + isSwipeCardStr);
}

void YomaniSdkWrapper::makeSale()
{
    amnt = _saleSettings._amount;
    cardBrand = _saleSettings._cardBrand;
    saleAction = _saleSettings._saleAction;
    merchRef = QString::fromStdString(_saleSettings._merchRef);
    this->actionTransaction();
}

void YomaniSdkWrapper::actionConnect()
{

    if(!terminalConnectionStatus){

        if (!serviceCreated) {
            createService();
        }

        startService();
    }
}

void YomaniSdkWrapper::deliveryAccepted()
{
    this->productDeliveryConfimation = ProductDeliveryBlockReturnValueAccepted;
}

void YomaniSdkWrapper::deliveryRejected()
{
    this->productDeliveryConfimation = ProductDeliveryBlockReturnValueRejected;
}

/////////////////////////////
// Utility
/////////////////////////////

std::string YomaniSdkWrapper::getTerminalID()
{
    return terminalID;//terminal->terminalId().data() + " (" + terminal->model().data() + ")";
}

boost::optional<std::string> YomaniSdkWrapper::getMerchantReference()
{
//    std::string str = ui->merchantRefTextEdit->text().toStdString();
    if (!merchRef.isEmpty()) {
        return merchRef.toStdString();
    } else {
        return boost::none;
    }
}

boost::optional<ect::ECTData> YomaniSdkWrapper::getDiscretionaryData()
{
//    std::string str = ui->discretionaryDataTextEdit->text().toStdString();
    return boost::optional<ect::ECTData>();/*str.empty() ? boost::optional<ect::ECTData>()
                       : ect::ECTData(Demo::HexUtil2::parse(str));*/
}

std::string YomaniSdkWrapper::getAcquirerID()
{
    return acquirerId.toStdString();
}

ect::Seconds YomaniSdkWrapper::cardRecognitionTimeout() const
{
    std::string str = cardRecoTimeout.toStdString();
    return Seconds(std::stoi(str));
}

Seconds YomaniSdkWrapper::nextCardRecognitionTimeout() const
{
    std::string str = pendingSaleTimeout.toStdString();
    return std::chrono::seconds(std::stoi(str));
}

void YomaniSdkWrapper::setSettings()
{
    ectSettings_.cardRecognitionTimeout = cardRecognitionTimeout();
    ectSettings_.cardInfoTrailingRequestTimeout = nextCardRecognitionTimeout();
    ectSettings_.currencyCode = "EUR";
    ectService_->updateSettings(ectSettings_);
    refreshSettings();
}

void splitAmount(std::string amount, int& unit, int& decimal)
{
    std::vector<std::string> strings;

    unit = 0;
    decimal = 0;

    // split the string
    std::istringstream f(amount.c_str());
    std::string s;
    while (getline(f, s, ',')) {
        strings.push_back(s);
    }
    if (strings.size() > 0)
        unit = stoi(strings[0]);
    if (strings.size() > 1) {
        std::string deciS = strings[1];
        if (deciS.length() == 1) {
            deciS += "0";
        }
        decimal = stoi(deciS);
    }
}

std::string formatLocalTime(boost::optional<tm> timePtr)
{
    if (timePtr) {
        tm timeToFormat = *timePtr;
        char buff[20];
        strftime(buff, 20, "%d-%m-%Y %H-%M-%S", &timeToFormat);
        return std::string(buff);
    }
    return "";
}

std::string convertCurrentTime()
{
    time_t now;
    time(&now);

    char buff[20];
    strftime(buff, 20, "%d-%m-%Y %H-%M-%S", localtime(&now));
    return std::string(buff);
}

QFileInfoList listTickets()
{
    QDir dir("tickets");
    if (!dir.exists()) {
        dir.mkpath(".");
    }

    QFileInfoList list = dir.entryInfoList(QDir::Filter::NoDotAndDotDot | QDir::Filter::Files, QDir::SortFlag::Name | QDir::SortFlag::Reversed);
    return list;
}

std::shared_ptr<CTEPTerminal> YomaniSdkWrapper::getTerminal(const std::string& id)
{
    return this->ectService_->getTerminal(id);
}

void YomaniSdkWrapper::actionTransaction()
{
    auto terminal = getTerminal(getTerminalID());
    if (!terminal) {
        debugLog("Terminl not found: " + getTerminalID());
        return;
    }

    switch (saleAction) {
    case ACTION_INDEX_CARD_VALIDITY_CHECK:
    case ACTION_INDEX_CARD_INFO:
    case ACTION_INDEX_CARD_INFO_WITH_SALE:
    case ACTION_INDEX_CANCEL:
    case ACTION_INDEX_SALE:
    case ACTION_INDEX_SALE_WITH_SMARTPAY:
    case ACTION_INDEX_CANCEL_SALE:
    case ACTION_INDEX_SALE_WITH_DELIVERY_CONFIMATION:
    case ACTION_INDEX_SALE_WITH_CASHBACK:
    case ACTION_INDEX_RESERVATION:
    case ACTION_INDEX_SALE_AFTER_RESERVATION:
    case ACTION_INDEX_CANCEL_RESERVATION:
    case ACTION_INDEX_CANCEL_SALE_AFTER_RESERVATION:
    case ACTION_INDEX_REFUND:
    case ACTION_INDEX_SALE_AFTER_VOICE_REFERRAL:
    case ACTION_INDEX_CASH_ADVANCE:
    case ACTION_INDEX_CANCEL_CASH_ADVANCE:
    case ACTION_INDEX_CASH_ADVANCE_AFTER_VOICE_REFERRAL:
    case ACTION_INDEX_TICKET_PRINT:
    case ACTION_INDEX_BALANCE_CONSULATION:
    case ACTION_INDEX_RESET_TRANSACTION:
    case ACTION_INDEX_LAST_TRANSACTION_STATUS:
    case ACTION_INDEX_BACKUP_MODE_ON:
    case ACTION_INDEX_BACKUP_MODE_OFF:
    case ACTION_INDEX_BACKUP_MODE_STATUS:
    case ACTION_INDEX_MMI_SHOW_MESSAGE:
    case ACTION_INDEX_MMI_RESET_SCREEN:
    case ACTION_INDEX_MMI_CHECK_PIN:
    case ACTION_INDEX_MMI_CHECK_PIN_WITH_RETRY:
    case ACTION_INDEX_MMI_CANCEL_CHECK_PIN_WITH_RETRY:
        if (terminal->connected()) {
            debugLog("---------------------------------");

            setSettings();

            switch (saleAction) {
            case ACTION_INDEX_CARD_VALIDITY_CHECK:
                debugLog("Making a Card Validity Check to Terminal: " + getTerminalID());
                actionCardValidityCheck(*terminal);
                break;

            case ACTION_INDEX_CARD_INFO:
                debugLog("Making a Card Info to Terminal: " + getTerminalID());
                actionCardInfo(CardInfoOption::NoSaleAfterRecognition, *terminal);
                break;

            case ACTION_INDEX_CARD_INFO_WITH_SALE:
                debugLog("Making a Card Info with sale pending to Terminal: " + getTerminalID());
                actionCardInfo(CardInfoOption::SaleAfterRecognition, *terminal);
                break;

            case ACTION_INDEX_CANCEL:
                debugLog("Cancel current transaction to Terminal: " + getTerminalID());
                actionRetailCancel(*terminal);
                break;

            case ACTION_INDEX_SALE:
                debugLog("Making a Sale to Terminal: " + getTerminalID());
                actionSale(SaleAction::Sale, *terminal);
                break;

            case ACTION_INDEX_SALE_WITH_SMARTPAY:
                debugLog("Making a Sale with SmartPay to Terminal: " + getTerminalID());
                actionSaleWithSmartPay(*terminal);
                break;

            case ACTION_INDEX_CANCEL_SALE:
                debugLog("Canceling a Sale to Terminal: " + getTerminalID());
                actionAuthorizedSale(AuthorizedSaleAction::CancelSale, *terminal);
                break;

            case ACTION_INDEX_SALE_WITH_DELIVERY_CONFIMATION:
                debugLog("Making a Sale with delivery confirmationto Terminal: " + getTerminalID());
                actionSaleWithDeliveryConfirmation(*terminal);
                break;

            case ACTION_INDEX_SALE_WITH_CASHBACK:
                debugLog("Making a Sale with cashback to Terminal: " + getTerminalID());
                actionSaleWithCashback(*terminal);
                break;

            case ACTION_INDEX_RESERVATION:
                debugLog("Making a Reservation to Terminal: " + getTerminalID());
                actionSale(SaleAction::Reservation, *terminal);
                break;

            case ACTION_INDEX_SALE_AFTER_RESERVATION:
                debugLog("Making a Sale after Reservation to Terminal: " + getTerminalID());
                actionAuthorizedSale(AuthorizedSaleAction::SaleAfterReservation, *terminal);
                break;

            case ACTION_INDEX_CANCEL_RESERVATION:
                debugLog("Canceling Reservation to Terminal: " + getTerminalID());
                actionAuthorizedSale(AuthorizedSaleAction::CancelReservation, *terminal);
                break;

            case ACTION_INDEX_CANCEL_SALE_AFTER_RESERVATION:
                debugLog("Canceling Sale after Reservation to Terminal: " + getTerminalID());
                actionAuthorizedSale(AuthorizedSaleAction::CancelSaleAfterReservation, *terminal);
                break;

            case ACTION_INDEX_REFUND:
                debugLog("Making Redund to Terminal: " + getTerminalID());
                actionSale(SaleAction::Refund, *terminal);
                break;

            case ACTION_INDEX_SALE_AFTER_VOICE_REFERRAL:
                debugLog("Making Sale after Voice Referral to Terminal: " + getTerminalID());
                actionAuthorizedSale(AuthorizedSaleAction::SaleAfterVoiceReferral, *terminal);
                break;

            case ACTION_INDEX_CASH_ADVANCE:
                debugLog("Making a Cash Advance to Terminal: " + getTerminalID());
                actionSale(SaleAction::CashAdvance, *terminal);
                break;

            case ACTION_INDEX_CANCEL_CASH_ADVANCE:
                debugLog("Canceling a Cash Advance to Terminal: " + getTerminalID());
                actionAuthorizedSale(AuthorizedSaleAction::CancelCashAdvance, *terminal);
                break;

            case ACTION_INDEX_CASH_ADVANCE_AFTER_VOICE_REFERRAL:
                debugLog("Making a Cash Advance after Voice Referral to Terminal: " + getTerminalID());
                actionAuthorizedSale(AuthorizedSaleAction::CashAdvanceAfterVoiceReferral, *terminal);
                break;

            case ACTION_INDEX_TICKET_PRINT:
                debugLog("Printing last transaction ticket to Terminal: " + getTerminalID());
                actionPrintTicket(*terminal);
                break;

            case ACTION_INDEX_BALANCE_CONSULATION:
                debugLog("Querying the Balance from Terminal: " + getTerminalID());
                actionBalance(*terminal);
                break;

            case ACTION_INDEX_RESET_TRANSACTION:
                debugLog("Reset state to Terminal: " + getTerminalID());
                terminal->resetTransaction();
                break;

            case ACTION_INDEX_LAST_TRANSACTION_STATUS:
                debugLog("Asks the status of last transaction for terminal: " + getTerminalID());
                actionAskLastTransactionStatus(*terminal);
                break;

            case ACTION_INDEX_BACKUP_MODE_ON:
                debugLog("Backup Mode ON for terminal: " + getTerminalID());
                actionBackupMode(true, *terminal);
                break;
            case ACTION_INDEX_BACKUP_MODE_OFF:
                debugLog("Backup Mode OFF for terminal: " + getTerminalID());
                actionBackupMode(false, *terminal);
                break;
            case ACTION_INDEX_BACKUP_MODE_STATUS:
                debugLog("Backup Mode STATUS for terminal: " + getTerminalID());
                actionBackupModeStatus(*terminal);
                break;

            case ACTION_INDEX_MMI_SHOW_MESSAGE:
                debugLog("Show Message for terminal: " + getTerminalID());
                actionShowMessage(*terminal);
                break;
            case ACTION_INDEX_MMI_RESET_SCREEN:
                debugLog("Reset screen for terminal: " + getTerminalID());
                actionResetScreen(*terminal);
                break;
            case ACTION_INDEX_MMI_CHECK_PIN:
                debugLog("Validate Password for terminal: " + getTerminalID());
                actionValidatePassword(*terminal);
                break;
            case ACTION_INDEX_MMI_CHECK_PIN_WITH_RETRY:
                debugLog("Validate Password With Retry for terminal: " + getTerminalID());
                actionValidatePasswordWithRetry(*terminal);
                break;
            case ACTION_INDEX_MMI_CANCEL_CHECK_PIN_WITH_RETRY:
                debugLog("\r\nCancel Validate Password With Retry for terminal: " + getTerminalID() + "\r\n");
                actionCancelValidatePasswordWithRetry(*terminal);
                break;
            }
        } else {
            QMessageBox msgbox;
            msgbox.setText("Terminal not connected");
            msgbox.exec();
        }
        break;

    default: {
        QMessageBox msgbox;
        msgbox.setText("Transaction not implemented yet");
        msgbox.exec();
    }
    }
}

template <>
void YomaniSdkWrapper::logResult<SaleWithCashbackResult>(const SaleWithCashbackResult& result)
{
    std::stringstream logStream;
    logStream << "SaleWithCashbackResult"
              << " success: " << result.authorizedAmount()
              << " EUR   |  auth. code: " << result.authorizationCode() << std::endl
              << "Timestamp: " << formatLocalTime(result.timeStamp()) << std::endl
              // << "Ticket:" << std::endl << result.clientTicket() << std::endl
              << "--------------------------------" << std::endl
              << std::endl;

    debugLogOnMainThread(logStream.str());
}

template <>
void YomaniSdkWrapper::handleSaleResult<SaleWithCashbackResult>(const SaleWithCashbackResult& result)
{
//    if (validateResult(result)) {
//        callOnGUIThread([this, result]() {
            logResult(result);
//            saveTicket(result.clientTicket(), result.merchantTicket(), "SaleWithCashbackResult");
//            ui->authCodeTextEdit->setText(result.authorizationCode().data());
//        });
//    }
}

int ectSaleSystemActionIdentifierLastByte = 0x00;
ECTSaleSystemActionIdentifier nextECTSaleSystemActionIdentifier()
{
    ectSaleSystemActionIdentifierLastByte++;
    return ECTSaleSystemActionIdentifier(0x01, 0x0A, 0x02, ectSaleSystemActionIdentifierLastByte);
}

void YomaniSdkWrapper::actionSale(SaleAction action, CTEPTerminal& terminal)
{
//    auto amount = Amount();

    ECTSaleSystemActionIdentifier ssai = nextECTSaleSystemActionIdentifier();
    m_log.write("In actionSale Function.");
    m_log.write("creating trans.");
    ect::SaleTransaction trans(action, amnt, getMerchantReference(), getDiscretionaryData(), ssai,
            [this](const SaleResult& result) {
                handleSaleResult(result);
            });

    auto filterInt = cardBrand;
    if (filterInt != -1) {
        CardBrandIdentifier filter = (CardBrandIdentifier)filterInt;
        trans = std::move(trans.addCardIdentifierFilter(filter));
    }

    auto propData = ECTData(HexUtil2::parse("AABBCCDDEEFF00112233445566"));
    trans.addProprietaryData(propData);

    m_log.write("making trans.");
    terminal.send(std::move(trans));
}

void YomaniSdkWrapper::actionSaleWithSmartPay(ect::CTEPTerminal& terminal)
{
    std::string merchantRef = getMerchantReference() ? getMerchantReference().get() : "test-ref";

    terminal.send(ect::SmartPaySaleTransaction(amnt, merchantRef,
        [this](const SaleResult& result) {
            handleSaleResult(result);
        }));
}

void YomaniSdkWrapper::actionAskLastTransactionStatus(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::LastTransactionStatusTransaction(
        [this](const LastTransactionResult& result) {
            handleSaleResultLastTransactionResult(result);
        }));
}

void YomaniSdkWrapper::actionSaleWithDeliveryConfirmation(ect::CTEPTerminal& terminal)
{
//    auto amount = Amount(ui->amountTextEdit->text().toStdString());

//    std::string txt = ui->deliveryTimeoutTextEdit->text().toStdString();
    int deliveryTimeout = -1;

    try {
//        deliveryTimeout = stoi();
        ectSettings_.deliveryTimeout = boost::none;
        ectService_->updateSettings(ectSettings_);
    } catch (...) {
//        ui->deliveryTimeoutTextEdit->setText(QString(""));
        ectSettings_.deliveryTimeout = boost::none;
        ectService_->updateSettings(ectSettings_);
    }

    terminal.send(ect::SaleWithDeliveryConfirmationTransaction(amnt, getMerchantReference(), getDiscretionaryData(), ECTSaleSystemActionIdentifier(0x01, 0x02, 0x03, 0x04),
        [this](const SaleResult& result) {
            handleSaleResult(result);
        },
        [this](const SaleResult& result) {
            std::cout << result.authorizedAmount() << std::endl;

            this->productDeliveryConfimation = boost::none;

//            callOnGUIThread([this]() {
//                this->ui->groupDelivery->setVisible(true);
//                this->ui->executeButton->setEnabled(false);
//            });

            while (!this->productDeliveryConfimation) {
                std::cout << "start sleep" << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                std::cout << "end sleep" << std::endl;
            }

            return this->productDeliveryConfimation.get();
        }));
}

void YomaniSdkWrapper::actionSaleWithCashback(ect::CTEPTerminal& terminal)
{
    Q_UNUSED(terminal)
    // First check if cashback is enabled.
//    terminal.send(ect::ServiceInformationTransaction([&, this](const ServiceInformationResult& result) {
//        if (result.cashbackEnabled()) {
//            // Cashback enabled, start the transaction
//            callOnGUIThread([&, this]() {
//                auto amount = Amount(ui->amountTextEdit->text().toStdString());
//                auto cashbackAmount = Amount(ui->cashbackAmountTextEdit->text().toStdString());

//                terminal.send(ect::SaleWithCashbackTransaction(amount, cashbackAmount, getMerchantReference(), getDiscretionaryData(),
//                    [this](const SaleWithCashbackResult& result) {
//                        handleSaleResult(result);
//                    }));

//            });

//        } else {
//            // Cashback not enabled,
//            debugLog("Sale with cashback KO : casback not enabled in the terminal !");
//        }
//    }));
}

void YomaniSdkWrapper::actionAuthorizedSale(AuthorizedSaleAction action, ect::CTEPTerminal& terminal)
{
//    auto amount = Amount(ui->amountTextEdit->text().toStdString());

    terminal.send(ect::AuthorizedSaleTransaction(action, amnt, getMerchantReference(), getDiscretionaryData(),
        "",
        [this](const AuthorizedSaleResult& result) {
            handleSaleResult(result);
        }));
//    terminal.send(ect::AuthorizedSaleTransaction(action, amnt, getMerchantReference(), getDiscretionaryData(),
//        ui->authCodeTextEdit->text().toStdString(),
//        [this](const AuthorizedSaleResult& result) {
//            handleSaleResult(result);
//        }));
}

void YomaniSdkWrapper::actionRetailCancel(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::CancelRetailTransaction([this](const CancelRetailResult& result) {
        handleBasicResult("CancelRetail", result);
    }));
}

void YomaniSdkWrapper::actionBackupMode(bool enable, ect::CTEPTerminal& terminal)
{
    terminal.send(ect::BackupModeTransaction(enable,
        [this](const BackupModeResult& result) {
            handleBackupModeResult(result);
        }));
}

void YomaniSdkWrapper::actionBackupModeStatus(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::BackupModeStatusTransaction(
        [this](const BackupModeStatusResult& result) {
            handleBackupModeStatusResult(result);
        }));
}

void YomaniSdkWrapper::actionCardValidityCheck(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::CheckCardValidityTransaction([this](const CheckCardValidityResult& result) {
        handleCardValidityResult(result);
    }));
}

void YomaniSdkWrapper::actionCardInfo(CardInfoOption cardInfoOption, ect::CTEPTerminal& terminal)
{
    terminal.send(ect::RequestCardInfoTransaction(cardInfoOption, [this](const RequestCardInfoResult& result) {
        handleCardInfoResult(result);
    }));
}

void YomaniSdkWrapper::actionPrintTicket(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::PrintTicketTransaction(ticketText, [this](const PrintTicketResult& result) {
        handleBasicResult("Print Ticket", result);
    }));
}

void YomaniSdkWrapper::actionBalance(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::BalanceTerminalConsultationTransaction(getAcquirerID(), [this](const BalanceTerminalConsultationResult& result) {
        handleBalanceConsultationResult(result);
    }));
}

void YomaniSdkWrapper::actionShowMessage(ect::CTEPTerminal& terminal)
{
    int screenID = 6;
    std::string languageCode = "en";
    int displayTimeout = 15;
    terminal.send(ect::ShowMessage(screenID, languageCode, displayTimeout, [this](const MMITransactionResult& result) {
        handleShowMessageResult(result);
    }));
}

void YomaniSdkWrapper::actionResetScreen(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::ResetScreen());
}

void YomaniSdkWrapper::actionValidatePassword(ect::CTEPTerminal& terminal)
{
    Q_UNUSED(terminal)
//    int screenID = 0;
//    std::string languageCode = "en";
//    int displayTimeout = 0;
//    int keyIndex = 0;

//    try {
//        screenID = stoi(ui->amountTextEdit->text().toStdString());
//        displayTimeout = stoi(ui->cardRecoTimeoutTextEdit->text().toStdString());
//        keyIndex = stoi(ui->salePendingTimeoutTextEdit->text().toStdString());
//    } catch (...) {
//        return;
//    }

//    EncryptedData encryptedPin(ui->acquirerIDTextEdit->text().toStdString(), keyIndex, ui->authCodeTextEdit->text().toStdString(), ui->discretionaryDataTextEdit->text().toStdString(), ect::ECTEncryptionAlgorithm::AES_CTR);

//    if (ui->merchantRefTextEdit->text().toStdString().length() > 0) {
//        FreeText freeText(ui->merchantRefTextEdit->text().toStdString(), "", "", "");

//        terminal.send(ect::VerifyPin(encryptedPin, screenID, languageCode, displayTimeout, freeText, [this](const VerifyPinResult& result) {
//            handleValidatePasswordResult(result);
//        }));
//    } else {
//        terminal.send(ect::VerifyPin(encryptedPin, screenID, languageCode, displayTimeout, [this](const VerifyPinResult& result) {
//            handleValidatePasswordResult(result);
//        }));
//    }
}

void YomaniSdkWrapper::actionValidatePasswordWithRetry(ect::CTEPTerminal& terminal)
{

    Q_UNUSED(terminal)
//    int maximumTries = 2;
//    std::string languageCode = "en";
//    int displayTimeout = 0;
//    int keyIndex = 0;

//    try {
//        displayTimeout = stoi(ui->cardRecoTimeoutTextEdit->text().toStdString());
//        keyIndex = stoi(ui->salePendingTimeoutTextEdit->text().toStdString());
//    } catch (...) {
//        return;
//    }

//    EncryptedData encryptedPin(ui->acquirerIDTextEdit->text().toStdString(), keyIndex, ui->authCodeTextEdit->text().toStdString(), ui->discretionaryDataTextEdit->text().toStdString(), ect::ECTEncryptionAlgorithm::AES_CTR);

//    ect::VerifyPinFlow trans(encryptedPin, languageCode, displayTimeout, maximumTries, true, [this](const VerifyPinResult& result) {
//        handleValidatePasswordWithRetryResult(result);
//    });

//    if (ui->merchantRefTextEdit->text().toStdString().length() > 0) {
//        FreeText freeText(ui->merchantRefTextEdit->text().toStdString(), "", "", "");
//        trans = std::move(trans.setFreeText(freeText));
//    }

//    FreeText freeTextSuccess("Great", "Thank you!", "", "");
//    trans = std::move(trans.setSuccessNextMessageData(3, 29, freeTextSuccess));

//    FreeText freeTextFail("Sorry", "You entered", "wrong", "pin");
//    trans = std::move(trans.setFailNextMessageData(3, 29, freeTextFail));

//    terminal.send(std::move(trans));
}

void YomaniSdkWrapper::actionCancelValidatePasswordWithRetry(ect::CTEPTerminal& terminal)
{
    terminal.send(ect::CancelPasswordValidation());
}

template <class T>
bool YomaniSdkWrapper::validateResult(const T& result)
{
    if (auto error = result.error()) {
        reportError(*error);
        return false;
    }
    return true;
}

void YomaniSdkWrapper::reportError(const ECTError& error)
{
    QString errorStr = QString::fromStdString(error.description());
    debugLog("Message sent with error: " + error.description());
    if(error.causeCode() && error.causeCode() == ECTErrorCauseCode::ResetTransaction) {
        debugLog("Transaction result uncertain, please check manually the transaction result !");
        errorStr += "\nTransaction result uncertain, please check manually the transaction result !";
    }
    debugLog("---------------------------------");
    if(serviceCallback!=nullptr){
        serviceCallback(static_cast<int>(WrapperInfo::transactionError),errorStr.toStdString().c_str() );
    }
}

/////////////////////////////
// Thread stuff
/////////////////////////////

void YomaniSdkWrapper::debugLog(std::string input)
{
    QString inputString = QString::fromStdString(input);
    m_log.write(inputString);
}

void YomaniSdkWrapper::debugLogOnMainThread(std::string input)
{
    QString inputString = QString::fromStdString(input);
    m_log.write(inputString);
}

void YomaniSdkWrapper::setConnectedTerminalId(std::shared_ptr<ect::CTEPTerminal> terminal)
{
    emit signalSetConnectedTerminalIdOnMainThread(terminal);
}

void YomaniSdkWrapper::setConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal> terminal)
{
    QString qs = QString(terminal->terminalId().data()) + " (" + QString(terminal->model().data()) + ")";

//    terminalID =

    setTerminalID(terminal->terminalId());
//    int index = ui->cmbTerminals->findData(QString::fromStdString(terminal->terminalId()));
//    if (index < 0) {
//        ui->cmbTerminals->addItem(qs, QVariant(QString::fromStdString(terminal->terminalId())));
//    }

    debugLog("terminal Id: " + terminal->terminalId());
}

void YomaniSdkWrapper::removeConnectedTerminalId(std::shared_ptr<ect::CTEPTerminal> terminal)
{
    emit signalRemoveConnectedTerminalIdOnMainThread(terminal);
}

void YomaniSdkWrapper::removeConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal> terminal)
{
//    int index = ui->cmbTerminals->findData(QString::fromStdString(terminal->terminalId()));
//    if (index >= 0) {
//        ui->cmbTerminals->removeItem(index);
//    }
}

void YomaniSdkWrapper::removeAllConnectedTerminalsId()
{
    emit signalRemoveAllConnectedTerminalsIdOnMainThread();
}

void YomaniSdkWrapper::removeAllConnectedTerminalsIdOnMainThread()
{
//    ui->cmbTerminals->clear();
}

void YomaniSdkWrapper::callOnGUIThread(std::function<void()> block)
{
    emit signalCallBlockOnMainThread(block);
}

void YomaniSdkWrapper::callBlockOnMainThread(callback block)
{
    block();
}

void YomaniSdkWrapper::createService()
{
    m_log.write("Inside Create Service");
    if(!serviceCreated)
    {
        debugLog("---------------------------------");
        debugLog("Starting Service");
        QString port = QString::fromStdString(serialPort);
        QString baud = QString::number(baudRate);
        m_log.write("Serial Port is : "+port+ " and BaudRate is : "+ baud);
        ectService_ = makeSerialCTEPService(serialPort, baudRate, *this);
        auto heartbeatInterval = HEARTBEAT_INT;
        Settings settings = ectService_->settings();
        settings.serialHeartBeatInterval = MilliSeconds(heartbeatInterval);
        ectService_->updateSettings(settings);
        if(serviceCallback)
        {
            debugLog("calling callback function");
            serviceCallback(static_cast<int>(WrapperInfo::info), "Created Service" );
        }
    }
    else
    {
        m_log.write("Service is already created.");
    }
    m_log.write("Leaving Create Service");

}

void YomaniSdkWrapper::startService()
{
    if(!terminalConnectionStatus)
    {
        debugLog("Starting service");
        createService();
        ectService_->startService();
    }

}

void YomaniSdkWrapper::stopService()
{
    if(serviceCreated && terminalConnectionStatus)
    {
        serviceCreated = false;
        debugLog("stopping service");
        ectService_->stopService();
    }

}

void YomaniSdkWrapper::handleSaleResultLastTransactionResult(const ect::LastTransactionResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            logResultLastTransactionResult(result);
        });
    }
}

void YomaniSdkWrapper::handleBackupModeResult(const ect::BackupModeResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            std::stringstream logStream;
            logStream << " success " << std::endl
                      << "--------------------------------" << std::endl
                      << std::endl;
            debugLogOnMainThread(logStream.str());

        });
    }
}

void YomaniSdkWrapper::handleBackupModeStatusResult(const ect::BackupModeStatusResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            std::stringstream logStream;
            logStream << " Backup mode: " << (result.enabled() ? "On" : "Off") << std::endl
                      << "--------------------------------" << std::endl
                      << std::endl;
            debugLogOnMainThread(logStream.str());

        });
    }
}

void YomaniSdkWrapper::handleShowMessageResult(const ect::MMITransactionResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            std::stringstream logStream;
            logStream << " Show message success " << std::endl
                      << " did press OK : " << (result.okKeyPressed() ? "Yes" : "NO") << std::endl
                      << " did press Cancel : " << (result.stopKeyPressed() ? "Yes" : "NO") << std::endl
                      << " Aborted : " << (result.aborted() ? "Yes" : "NO") << std::endl
                      << "--------------------------------" << std::endl
                      << std::endl;
            debugLogOnMainThread(logStream.str());

        });
    }
}

void YomaniSdkWrapper::handleValidatePasswordResult(const ect::VerifyPinResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            std::stringstream logStream;
            logStream << " success " << std::endl
                      << " attempts left: " << result.attemptsLeft() << std::endl
                      << "--------------------------------" << std::endl
                      << std::endl;
            debugLogOnMainThread(logStream.str());

        });
    }
}

void YomaniSdkWrapper::handleValidatePasswordWithRetryResult(const ect::VerifyPinResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            std::stringstream logStream;
            logStream << " success " << std::endl
                      << " attempts left: " << result.attemptsLeft() << std::endl
                      << "--------------------------------" << std::endl
                      << std::endl;
            debugLogOnMainThread(logStream.str());

        });
    }
}

bool YomaniSdkWrapper::getTerminalConnectionStatus() const
{
    return terminalConnectionStatus;
}

bool YomaniSdkWrapper::getServiceCreated() const
{
    return serviceCreated;
}

SalesInfo YomaniSdkWrapper::getSaleSettings() const
{
    return _saleSettings;
}

void YomaniSdkWrapper::setSaleSettings(const SalesInfo &saleSettings)
{
    _saleSettings = saleSettings;
}

bool YomaniSdkWrapper::detectTerminal()
{
    createService();
    ectService_->startService();;
    QThread::sleep(5);
    if(!terminalConnectionStatus)
    {
        stopService();
    }
    return terminalConnectionStatus;
}

QString YomaniSdkWrapper::getPendingSaleTimeout() const
{
    return pendingSaleTimeout;
}

void YomaniSdkWrapper::setPendingSaleTimeout(const QString &value)
{
    pendingSaleTimeout = value;
}

QString YomaniSdkWrapper::getCardRecoTimeout() const
{
    return cardRecoTimeout;
}

void YomaniSdkWrapper::setCardRecoTimeout(const QString &value)
{
    cardRecoTimeout = value;
}

QString YomaniSdkWrapper::getAcquirerId() const
{
    return acquirerId;
}

void YomaniSdkWrapper::setAcquirerId(const QString &value)
{
    acquirerId = value;
}

QString YomaniSdkWrapper::getMerchRef() const
{
    return merchRef;
}

void YomaniSdkWrapper::setMerchRef(const QString &value)
{
    merchRef = value;
}

int YomaniSdkWrapper::getCardBrand() const
{
    return cardBrand;
}

void YomaniSdkWrapper::setCardBrand(int value)
{
    cardBrand = value;
}

int YomaniSdkWrapper::getSaleAction() const
{
    return saleAction;
}

void YomaniSdkWrapper::setSaleAction(int value)
{
    saleAction = value;
}

int YomaniSdkWrapper::getBaudRate() const
{
    return baudRate;
}

void YomaniSdkWrapper::setBaudRate(int value)
{
    baudRate = value;
}

void YomaniSdkWrapper::setTerminalID(const std::string &value)
{
    m_log.write(QString("Setting terminal ID to %1").arg(QString::fromStdString(value)));
    terminalID = value;
}

std::string YomaniSdkWrapper::getSerialPort() const
{
    return serialPort;
}

void YomaniSdkWrapper::setSerialPort(const std::string &value)
{
    m_log.write("inside setSerialPort function\n");
    QString port = QString::fromStdString(value);
    serialPort = port.toStdString();
    m_log.write("leaving setSerialPort function\n");
}

template <class T>
void YomaniSdkWrapper::handleSaleResult(const T& result)
{
    if (validateResult(result)) {
        bool magneticStripe = result.isMagneticStripe();
        bool chip = result.isIccEmv();
        std::stringstream message;
        message << "Card Info success: " << std::endl
                << "Card Brand Identifier: " << result.cardBrandIdentifier() << std::endl
                << "Card Brand Name: " << result.cardBrandName() << std::endl
                << "Entry mode: " << (chip ? "chip" : magneticStripe ? "magnetic stripe" : "other") << std::endl
                << "ClippedPAN: " << result.clippedPAN() << std::endl
                << "Auth Code: " << result.authorizationCode() << std::endl
                << "Timestamp: " << formatLocalTime(result.timeStamp()) << std::endl
                << "---------------------------------" << std::endl
                << "CLIENT TICKET "   << std::endl
                << result.clientTicket() << std::endl
                << std::endl;

        if(serviceCallback!=nullptr){
            serviceCallback(static_cast<int>(WrapperInfo::transactionSuccess),message.str().c_str());
        }
        debugLog(message.str());
    }
}

void YomaniSdkWrapper::logResultLastTransactionResult(const ect::LastTransactionResult& result)
{
    std::stringstream logStream;
    logStream << "Incident code: " << result.incidentCode() << std::endl
              << " saleSystemActionIdentifier: " << (result.saleSystemActionIdentifier() ? result.saleSystemActionIdentifier()->data.stringValue() : "none") << std::endl
              << " amount: " << result.authorizedAmount()
              << " EUR   |  auth. code: " << result.authorizationCode() << std::endl
              << "Timestamp: " << formatLocalTime(result.timeStamp()) << std::endl
              << "--------------------------------" << std::endl
              << std::endl;

    debugLogOnMainThread(logStream.str());
}

template <class T>
void YomaniSdkWrapper::logResult(const T& result)
{
    debugLog("In LogResult Func:");
    std::stringstream logStream;
    logStream << toString(result.action())
              << " success: " << result.authorizedAmount()
              << " EUR   |  auth. code: " << result.authorizationCode() << std::endl
              << "Timestamp: " << formatLocalTime(result.timeStamp()) << std::endl
              << " saleSystemActionIdentifier: " << (result.saleSystemActionIdentifier() ? result.saleSystemActionIdentifier()->data.stringValue() : "none") << std::endl
              << " card brand identifier: " << result.cardBrandIdentifier() << std::endl
              << " card brand name: " << result.cardBrandName() << std::endl
              << " processed in backup mode: " << (result.processedInBackupMode() ? "yes" : "false") << std::endl
              << "Ticket:" << std::endl
              << result.clientTicket() << std::endl
              << "--------------------------------" << std::endl
              << std::endl;


    debugLogOnMainThread(logStream.str());
}

template <class T>
void YomaniSdkWrapper::handleBasicResult(const std::string& action, const T& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result, action]() {
            std::stringstream message;
            message << action << " success: " << std::endl
                    //<< "Timestamp: " << formatLocalTime(result.timeStamp()) << std::endl
                    << "---------------------------------" << std::endl
                    << std::endl;

            debugLogOnMainThread(message.str());
        });
    }
}

void YomaniSdkWrapper::handleCardInfoResult(const RequestCardInfoResult& result)
{
    if (validateResult(result)) {

        bool magneticStripe = result.isMagneticStripe();
        bool chip = result.isIccEmv();
        std::stringstream message;
        message << "Card Info success: " << std::endl
                << "\tCard Brand Identifier: " << result.cardBrandIdentifier() << std::endl
                << "\tCard Brand Name: " << result.cardBrandName() << std::endl
                << "\tEntry mode: " << (chip ? "chip" : magneticStripe ? "magnetic stripe" : "other") << std::endl
                << "\tClippedPAN: " << result.clippedPAN() << std::endl
                << "\tToken: " << result.token() << std::endl
                << "Timestamp: " << formatLocalTime(result.timeStamp()) << std::endl
                << "---------------------------------" << std::endl
                << std::endl;
        if(serviceCallback!=nullptr){
            serviceCallback(static_cast<int>(WrapperInfo::transactionSuccess),message.str().c_str());
        }
        debugLog(message.str());
    }
}

void YomaniSdkWrapper::handleCardValidityResult(const CheckCardValidityResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            std::stringstream message;
            message << "Check Card Validity success: " << std::endl
                    << "\tClippedPAN: " << result.clippedPAN() << "  |  " << result.cardBrandName() << std::endl
                    << "Timestamp: " << formatLocalTime(result.timeStamp()) << std::endl
                    << "---------------------------------" << std::endl
                    << std::endl;
            debugLogOnMainThread(message.str());
        });
    }
}

void YomaniSdkWrapper::handleBalanceConsultationResult(const ect::BalanceTerminalConsultationResult& result)
{
    if (validateResult(result)) {
        callOnGUIThread([this, result]() {
            std::stringstream message;

            message << "Balance querying success: " << std::endl
                    << "  acquirer ID: " << result.acquirerId() << std::endl
                    << "  terminal ID: " << result.terminalId() << std::endl
                    <<

                "  Financial counters: " << std::endl;
            for (auto fci : result.financialCounterInfo()) {
                message << "    --- " << std::endl
                        << "    identifier             : " << fci.identifier() << std::endl
                        << "    currencyCode           : " << fci.currencyCode() << std::endl
                        << "    name                   : " << fci.name() << std::endl
                        << "    transactionsNumber     : " << fci.transactionsNumber() << std::endl
                        << "    cumulativeTotalAmount  : " << fci.cumulativeTotalAmount() << std::endl
                        << "    periodNumber           : " << fci.periodNumber() << std::endl
                        << "    cardBrandIdentifier    : " << fci.cardBrandIdentifier() << std::endl
                        << "    cardBrandName          : " << fci.cardBrandName() << std::endl
                        << "    terminalIdentifier     : " << fci.terminalIdentifier() << std::endl
                        << "    periodOpeningDate      : " << formatLocalTime(fci.periodOpeningDate()) << std::endl
                        << "    periodClosingDate      : " << formatLocalTime(fci.periodClosingDate()) << std::endl
                        << "    accountedServices      : " << fci.accountedServicesDescription() << std::endl
                        << "      Sale                     : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::saleService) ? "Yes" : "No") << std::endl
                        << "      Cash advance             : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::cacheAdvanceService) ? "Yes" : "No") << std::endl
                        << "      Reservation              : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::reservationService) ? "Yes" : "No") << std::endl
                        << "      Sale after reservation   : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::saleAfterReservationService) ? "Yes" : "No") << std::endl
                        << "      Card Validity Check      : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::cardValidityCheckService) ? "Yes" : "No") << std::endl
                        << "      Refund                   : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::refundService) ? "Yes" : "No") << std::endl
                        << "      Deferred Sale            : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::deferredSaleService) ? "Yes" : "No") << std::endl
                        << "      Extra on Sale            : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::extraOnSaleService) ? "Yes" : "No") << std::endl
                        << "      Cancellation             : " << (fci.isServiceLinked(ECTFinancialCounterInfoAccountedServices::cancellationService) ? "Yes" : "No") << std::endl;
                message << "    --- " << std::endl
                        << std::endl;
            }

            debugLogOnMainThread(message.str());
        });
    }
}




void createYomaniService(const char *port, int baudrate)
{
    std::string portname = port;
    if(globalWrapper == nullptr)
    {
        globalWrapper = new YomaniSdkWrapper();
        globalWrapper->debugLog("created global wrapper");
    }

    m_log.write("trying to access value of port name : ");
    m_log.write(QString::fromStdString(portname));

    globalWrapper->setSerialPort(portname);
    m_log.write("setSerialPort function called...");
    globalWrapper->setBaudRate(baudrate);
    m_log.write("setBaudRate function called...");
    globalWrapper->actionConnect();

}

void startYomaniService()
{
    if(globalWrapper == nullptr)
    {
        return;
    }

    globalWrapper->debugLog("Calling connect function");
    globalWrapper->startService();
}

void stopYomaniService()
{
    if(globalWrapper == nullptr)
    {
        return;
    }
    globalWrapper->debugLog("Stopping the service");
    delete globalWrapper;
    globalWrapper = nullptr;
//    globalWrapper->stopService();
}

void setChannelSettings()
{

}

void makeTransaction()
{
    if(globalWrapper != nullptr)
    {
        m_log.write("making a sales transaction.");
        globalWrapper->makeSale();
        m_log.write("finished sales transaction.");
    }

}

bool detectYomaniService(const char *port, int baudrate)
{
    std::string portname = port;
    QString portName = QString::fromStdString(portname);
    if(globalWrapper == nullptr)
    {
        globalWrapper = new YomaniSdkWrapper();
        globalWrapper->debugLog("created global wrapper");
    }
    m_log.write("Detecting Yomani terminal on port : " + portName);

    globalWrapper->setSerialPort(portname);
    globalWrapper->setBaudRate(baudrate);

    m_log.write("calling detect function of globalWrapper");
    bool res = globalWrapper->detectTerminal();
    if(!res){
        delete globalWrapper;
        globalWrapper = nullptr;
    }
    return res;
}


bool setCallback(SDKCallback pCb)
{
    m_log.write("Setting Call back function.");
    serviceCallback = pCb;
    if(pCb != nullptr)
    {
        m_log.write("Calling the call back function...");
        serviceCallback(100,"Test For callback");
        m_log.write("EOF setCallback Function.");
        return true;
    }
    else{
        return false;
    }
}

void setSalesSettings(double amount, int saleAction, int cardBrand, const char *merchantReference)
{
    SalesInfo settings;
    QString amountStr = QString::number(amount);
    ect::Amount amntToCharge(amountStr.toStdString());
    m_log.write("In setSalesSettings function :");
    QString log = QString("Amount :%1 \nSaleAction num : %2 \n Card Brand num : %3 \n").arg(amount).arg(saleAction).arg(cardBrand);
    m_log.write(log);
    settings._amount = amntToCharge;
    settings._saleAction = saleAction;
    settings._cardBrand = cardBrand;
    settings._merchRef = merchantReference;

    globalWrapper->setSaleSettings(settings);

}

QT_END_NAMESPACE
