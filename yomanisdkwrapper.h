#ifndef YOMANISDKWRAPPER_H
#define YOMANISDKWRAPPER_H
#include <QObject>
#include "yomanisdkwrapperInterface.h"
#include "logger.h"

#include <boost/optional.hpp>

#include "EasyCTEPCpp.h"

#define HEARTBEAT_INT                                   5000
#define ACTION_INDEX_CARD_VALIDITY_CHECK                0
#define ACTION_INDEX_CARD_INFO                          1
#define ACTION_INDEX_CARD_INFO_WITH_SALE                2
#define ACTION_INDEX_CANCEL                             3
#define ACTION_INDEX_SALE                               5
#define ACTION_INDEX_SALE_WITH_SMARTPAY                 6
#define ACTION_INDEX_CANCEL_SALE                        7
#define ACTION_INDEX_SALE_WITH_DELIVERY_CONFIMATION     9
#define ACTION_INDEX_SALE_WITH_CASHBACK                 10
#define ACTION_INDEX_RESERVATION                        11
#define ACTION_INDEX_SALE_AFTER_RESERVATION             12
#define ACTION_INDEX_CANCEL_RESERVATION                 13
#define ACTION_INDEX_CANCEL_SALE_AFTER_RESERVATION      14
#define ACTION_INDEX_REFUND                             16
#define ACTION_INDEX_SALE_AFTER_VOICE_REFERRAL          18
#define ACTION_INDEX_CASH_ADVANCE                       20
#define ACTION_INDEX_CANCEL_CASH_ADVANCE                21
#define ACTION_INDEX_CASH_ADVANCE_AFTER_VOICE_REFERRAL  22
#define ACTION_INDEX_TICKET_PRINT                       24
#define ACTION_INDEX_BALANCE_CONSULATION                26
#define ACTION_INDEX_RESET_TRANSACTION                  28
#define ACTION_INDEX_LAST_TRANSACTION_STATUS            30
#define ACTION_INDEX_BACKUP_MODE_ON                     32
#define ACTION_INDEX_BACKUP_MODE_OFF                    33
#define ACTION_INDEX_BACKUP_MODE_STATUS                 34
#define ACTION_INDEX_MMI_SHOW_MESSAGE                   36
#define ACTION_INDEX_MMI_RESET_SCREEN                   37
#define ACTION_INDEX_MMI_CHECK_PIN                      38
#define ACTION_INDEX_MMI_CHECK_PIN_WITH_RETRY           39
#define ACTION_INDEX_MMI_CANCEL_CHECK_PIN_WITH_RETRY    40

namespace ect
{
class ECTError;
class Service;
class SaleResult;
class AuthorizedSaleResult;
class RequestCardInfoResult;
class Terminal;

enum class AuthorizedSaleAction;
enum class AuthorizedSaleAction;
}

typedef struct SaleInfo{
    explicit SaleInfo();
    ect::Amount _amount;
    int _saleAction;
    int _cardBrand;
    std::string _merchRef;
} SalesInfo;


typedef std::function<void (WrapperInfo,std::string)> StringCallback;

typedef bool (*SDKCallback)(int,const char*);

class YomaniSdkWrapper: public QObject, public ect::CTEPServiceListener
{
    Q_OBJECT

public:
    explicit YomaniSdkWrapper();
    ~YomaniSdkWrapper() override;

    void debugLog(std::string input);
    void setConnectedTerminalId(std::shared_ptr<ect::CTEPTerminal>);
    void removeConnectedTerminalId(std::shared_ptr<ect::CTEPTerminal>);
    void removeAllConnectedTerminalsId();
    void callOnGUIThread(std::function<void()> block);

    void onServiceStart() override;
    void onServiceStop() override;
    void onServiceError(const ect::ECTError& error) override;
    void onTerminalConnect(std::shared_ptr<ect::CTEPTerminal> terminal) override;
    void onTerminalDisconnect(std::shared_ptr<ect::CTEPTerminal> terminal) override;
    void onTerminalEvent(const ect::ECTTerminalEvent terminalEvent) override;

    void makeSale();

    typedef std::function<void()> callback;

    std::string getSerialPort() const;
    void setSerialPort(const std::string &value);

    void setTerminalID(const std::string &value);

    int getBaudRate() const;
    void setBaudRate(int value);

    int getSaleAction() const;
    void setSaleAction(int value);

    int getCardBrand() const;
    void setCardBrand(int value);

    QString getMerchRef() const;
    void setMerchRef(const QString &value);

    QString getAcquirerId() const;
    void setAcquirerId(const QString &value);

    QString getCardRecoTimeout() const;
    void setCardRecoTimeout(const QString &value);

    QString getPendingSaleTimeout() const;
    void setPendingSaleTimeout(const QString &value);

    bool detectTerminal();

    SalesInfo getSaleSettings() const;
    void setSaleSettings(const SalesInfo &saleSettings);

    void startService();
    void stopService();
    bool getServiceCreated() const;

    bool getTerminalConnectionStatus() const;

public slots:
    void actionConnect();
    void actionTransaction();

    void deliveryAccepted();
    void deliveryRejected();

    void debugLogOnMainThread(std::string input);
    void setConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal> terminal);
    void removeConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal> terminal);
    void removeAllConnectedTerminalsIdOnMainThread();
    void callBlockOnMainThread(callback);

signals:
    void signalDebugLogOnMainThread(std::string input);
    void signalSetConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal> terminal);
    void signalRemoveConnectedTerminalIdOnMainThread(std::shared_ptr<ect::CTEPTerminal> terminal);
    void signalRemoveAllConnectedTerminalsIdOnMainThread();
    void signalCallBlockOnMainThread(callback);

private:
    bool isListening;

    std::string ticketText;

    void createService();
    bool isTerminalConnected(std::string terminalIdentifier);

    std::shared_ptr<ect::CTEPTerminal> getTerminal(const std::string &id);
    std::string getTerminalID();
    boost::optional<std::string> getMerchantReference();
    std::string getAcquirerID();

    boost::optional<ect::ECTData> getDiscretionaryData();
    std::shared_ptr<ect::CTEPService> ectService_;
    ect::Settings ectSettings_;

    ect::Seconds cardRecognitionTimeout() const;
    ect::Seconds nextCardRecognitionTimeout() const;

    void setSettings();
    void refreshSettings();

    void actionSale(ect::SaleAction action, ect::CTEPTerminal& terminal);
    void actionSaleWithSmartPay(ect::CTEPTerminal& terminal);
    void actionSaleWithDeliveryConfirmation(ect::CTEPTerminal& terminal);
    void actionSaleWithCashback(ect::CTEPTerminal& terminal);
    void actionAuthorizedSale(ect::AuthorizedSaleAction action, ect::CTEPTerminal& terminal);
    void actionRetailCancel(ect::CTEPTerminal& terminal);
    void actionBackupMode(bool enable, ect::CTEPTerminal& terminal);
    void actionBackupModeStatus(ect::CTEPTerminal& terminal);
    void actionCardValidityCheck(ect::CTEPTerminal& terminal);
    void actionCardInfo(ect::CardInfoOption cardInfoOption, ect::CTEPTerminal& terminal);
    void actionBalance(ect::CTEPTerminal& terminal);
    void actionPrintTicket(ect::CTEPTerminal& terminal);
    void actionAskLastTransactionStatus(ect::CTEPTerminal& terminal);
    void actionShowMessage(ect::CTEPTerminal& terminal);
    void actionResetScreen(ect::CTEPTerminal& terminal);
    void actionValidatePassword(ect::CTEPTerminal& terminal);
    void actionValidatePasswordWithRetry(ect::CTEPTerminal& terminal);
    void actionCancelValidatePasswordWithRetry(ect::CTEPTerminal& terminal);

    void reportError(const ect::ECTError &error);
    template <class T> bool validateResult(const T& result);
    template<class T> void handleSaleResult(const T& result);
    template<class T> void handleBasicResult(const std::string& action, const T& result);
    template<class T> void logResult(const T& result);
    void handleCardInfoResult(const ect::RequestCardInfoResult& result);
    void handleCardValidityResult(const ect::CheckCardValidityResult& result);
    void handleBalanceConsultationResult(const ect::BalanceTerminalConsultationResult& result);

    void handleSaleResultLastTransactionResult(const ect::LastTransactionResult& result);
    void logResultLastTransactionResult(const ect::LastTransactionResult& result);
    void handleBackupModeResult(const ect::BackupModeResult& result);
    void handleBackupModeStatusResult(const ect::BackupModeStatusResult& result);
    void handleShowMessageResult(const ect::MMITransactionResult& result);
    void handleValidatePasswordResult(const ect::VerifyPinResult& result);
    void handleValidatePasswordWithRetryResult(const ect::VerifyPinResult& result);


    boost::optional<ProductDeliveryBlockReturnValue> productDeliveryConfimation;

    std::string serialPort;
    std::string terminalID;
    int baudRate;

    int saleAction;

    int cardBrand;

    ect::Amount  amnt;

    bool serviceCreated;
    bool terminalConnectionStatus;

    QString merchRef;
    QString acquirerId;
    QString cardRecoTimeout;
    QString pendingSaleTimeout;

    SalesInfo _saleSettings;

};

static YomaniSdkWrapper *globalWrapper = nullptr;

static SDKCallback serviceCallback = nullptr;

static Logger m_log("debugLog.txt");

extern "C" YOMANISDKWRAPPERSHARED_EXPORT bool detectYomaniService(const char* port, int baudrate);

extern "C" YOMANISDKWRAPPERSHARED_EXPORT void createYomaniService(const char* port, int baudrate);

extern "C" YOMANISDKWRAPPERSHARED_EXPORT  bool setCallback(SDKCallback pCb);

extern "C" YOMANISDKWRAPPERSHARED_EXPORT  void startYomaniService();

extern "C" YOMANISDKWRAPPERSHARED_EXPORT  void stopYomaniService();

extern "C" YOMANISDKWRAPPERSHARED_EXPORT  void setChannelSettings();

extern "C" YOMANISDKWRAPPERSHARED_EXPORT  void setSalesSettings(double amount, int saleAction, int cardBrand, const char *merchantReference);

extern "C" YOMANISDKWRAPPERSHARED_EXPORT  void makeTransaction();



#endif // YOMANISDKWRAPPER_H
