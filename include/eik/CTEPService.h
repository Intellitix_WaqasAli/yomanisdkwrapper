#pragma once

#include <string>
#include <vector>
#include <memory>
#include "CTEPTerminal.h"

namespace ect {

struct Settings;

/*!
 * \brief The CTEPService interface is used to start and stop the full service gateway stack and to configure it
 */
class CTEPService {
public:
    virtual ~CTEPService() {}

    /*!
     * \brief Start listening for connected terminals
     */
    virtual void startService() = 0;

    /*!
     * \brief Stop the service
     *          This function does not destroy the service.
     *          Restarting the service is possible by call startService again.
     */
    virtual void stopService() = 0;

    /*!
     * \brief Get the current service settings
     * \return The current service settings
     */
    virtual Settings settings() const = 0;

    /*!
     * \brief Update the current service settings
     * \param settings The new service settings
     */
    virtual void updateSettings(const Settings& settings) = 0;

    /*!
     * \brief Return an iterable list containing the connected terminals.
     * \return an iterable list containing the connected terminals.
     */
    virtual std::vector<std::shared_ptr<CTEPTerminal>> connectedTerminals() = 0;

    /*!
     * \bried Returns the connected terminal matching the given TID.
     * \param tid the TID of the terminal
     * \return the connected terminal matching the TID, null if not found
     */
    virtual std::shared_ptr<CTEPTerminal> getTerminal(std::string terminalIdentifier) = 0;

protected:
    CTEPService() = default;
    CTEPService(const CTEPService&) = delete;
    CTEPService& operator=(const CTEPService&) = delete;
};
}
