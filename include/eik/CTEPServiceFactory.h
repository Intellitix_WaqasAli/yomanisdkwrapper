#pragma once

#include "easyctepdefs.h"
#include <memory>
#include <string>
#include <vector>


namespace ect {

class CTEPServiceListener;
class CTEPService;

/*!
 * \brief MakeTcpIpCTEPService
 *        This factory creates your service instance
 * \param listener Your ServiceListener instance
 * \return A new Service instance, nullptr if failed
 */

std::shared_ptr<CTEPService> makeTcpIpCTEPService(int listenPort, CTEPServiceListener& listener);

/*!
 * \brief MakeSerialCTEPService
 *        This factory creates your service instance
 * \param listenPort the COM port of the serial connection
 * \param listener Your ServiceListener instance
 * \return A new Service instance, nullptr if failed
 */

std::shared_ptr<CTEPService> makeSerialCTEPService(const std::string& listenPort, CTEPServiceListener& listener);

/*!
 * \brief MakeSerialCTEPService
 *        This factory creates your service instance
 * \param listenPort the COM port of the serial connection
 * \param baudRate the baud rate of the serial connection
 * \param listener Your ServiceListener instance
 * \return A new Service instance, nullptr if failed
 */

std::shared_ptr<CTEPService> makeSerialCTEPService(const std::string& listenPort, const int baudRate, CTEPServiceListener& listener);


/*!
 * \brief getAvailableSerialPorts
 * \return A list of available serial ports for serial connection.
 */

std::vector<std::string> getAvailableSerialPorts();


/*!
 * \brief Return an iterable list containing the connected terminals for all existing services.
 * \return an iterable list containing the connected terminals for all existing services.
 */
std::vector<std::shared_ptr<CTEPTerminal>> connectedTerminals();

/*!
 * \bried Returns the connected terminal matching the given TID for all existing services.
 * \param tid the TID of the terminal
 * \return the connected terminal matching the TID, null if not found
 */
std::shared_ptr<CTEPTerminal> getTerminal(std::string terminalIdentifier);

}
