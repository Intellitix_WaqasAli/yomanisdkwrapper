#pragma once

#include "eik/CTEPTerminal.h"
#include <map>
#include <vector>
#include <memory>

namespace ect {

class CTEPServiceListener;

class ConnectedTerminalsList
{
public:
    ~ConnectedTerminalsList();

    static ConnectedTerminalsList& getInstance();

    void add(std::shared_ptr<ect::CTEPTerminal> terminal, const ect::CTEPServiceListener* lstn);
    void remove(std::shared_ptr<ect::CTEPTerminal> terminal, const ect::CTEPServiceListener* lstn);
    void removeAll(const ect::CTEPServiceListener* lstn);

    std::vector<std::shared_ptr<CTEPTerminal>> connectedTerminals() const;
    std::vector<std::shared_ptr<CTEPTerminal>> connectedTerminalsFor(const ect::CTEPServiceListener& lstn) const;
    std::shared_ptr<CTEPTerminal> getTerminal(const std::string& terminalIdentifier) const;

private:
    ConnectedTerminalsList();
    ConnectedTerminalsList(ConnectedTerminalsList const&) = delete;
    void operator=(ConnectedTerminalsList const&)  = delete;

    std::map< const ect::CTEPServiceListener*,
              std::map <std::string, std::shared_ptr<ect::CTEPTerminal> >
    > connectedTerminalsCache;
};
}
