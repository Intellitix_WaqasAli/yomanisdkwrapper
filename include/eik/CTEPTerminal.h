#pragma once

#include <string>
#include <functional>

namespace ect {

class Transaction;

class CTEPTerminal {
public:
    virtual ~CTEPTerminal() {}

    /*!
     * \brief Get the terminal identifier
     * \return The terminal identifier
     */
    virtual std::string terminalId() const = 0;
    /*!
     * \brief Get the device type of the terminal
     * \return The device type
     */
    virtual std::string deviceType() const = 0;
    /*!
     * \brief Get the model name of the terminal
     * \return The model name
     */
    virtual std::string model() const = 0;
    /*!
     * \brief Get the serial number of the terminal
     * \return The serial number
     */
    virtual std::string serialNumber() const = 0;
    /*!
     * \brief Get the software version of the terminal
     * \return  The software version
     */
    virtual std::string softwareVersion() const = 0;
    /*!
     * \brief Get the EMV kernel version of the terminal
     * \return The EMV kernel version
     */
    virtual std::string EMVKernelVersion() const = 0;

    /*!
     * \brief Is the terminal connected
     * \return True if the terminal is connected. False otherwise
     */
    virtual bool connected() const = 0;

    /*!
     * \brief Reset any open transaction
     */
    virtual void resetTransaction() = 0;

    /*!
     * \brief Enable/disable the notifications of the terminal.
     * \param enable when true the notifications will be sent, when false the notifications will not be sent
     */
    virtual void enableNotifications(bool enable) = 0;

    /*!
     * \brief Gets a string description of the connection in json format
     * \return json connection description
     */
    virtual std::string getConnectionDescription() const = 0;

    /*!
     * \brief Make a transaction by sending a transaction object
     * \param transaction The transaction object
     */
    virtual void send(Transaction&& command) = 0;


    virtual void isAlive(std::function<void (const bool)> resultCallback) = 0;

protected:
    CTEPTerminal() = default;
    CTEPTerminal(const CTEPTerminal&) = delete;
    CTEPTerminal& operator=(const CTEPTerminal&) = delete;
};
}
