#pragma once

#include "ECT/ECTTerminalEvent.hpp"
#include <memory>

namespace ect {

class ECTError;
class CTEPTerminal;
class ECTTerminalEvent;

/*!
 * \brief The ServiceListener interface is used to listen for connecting terminals.
 * The main usage of the terminal instance is to make transactions. It is best to store the terminal somewhere for later usage.
 */

/*!
 \par Implementing a service listener
 You need to implement the Service listener interface:
 @code
 class MainWindow : public ect::ServiceListener
 {
 public:
    void onServiceStart() override;
    void onServiceStop() override;
    void onServiceError(const ect::ECTError& error) override;
    void onTerminalConnect(ConnectedTerminal terminal) override;
    void onTerminalDisconnect(ConnectedTerminal terminal) override;
 };
 @endcode
 */

class CTEPServiceListener {
public:
    virtual ~CTEPServiceListener() {}
    /*!
     * \brief Called if the service is started
     */
    virtual void onServiceStart() = 0;
    /*!
     * \brief Called if the service is stopped
     */
    virtual void onServiceStop() = 0;
    /*!
     * \brief Called if an service error has occured
     * \param error The error object containing error information
     */
    virtual void onServiceError(const ECTError& error) = 0;
    /*!
     * \brief Called if a new terminal connects
     * \param terminal The new terminal instance
     */

    /*!
     * \brief Called when a terminal sent an event
     * \param terminalEvents The events type the terminal sent
     */
    virtual void onTerminalEvent(const ECTTerminalEvent /* terminalEvents */){};

    /*!
     * \brief Called if a terminal is connected
     * \param terminal The connected terminal
     */
    virtual void onTerminalConnect(std::shared_ptr<CTEPTerminal> terminal) = 0;
    /*!
     * \brief Called if a terminal is disconnected
     * \param terminal The disconnected terminal instance
     */
    virtual void onTerminalDisconnect(std::shared_ptr<CTEPTerminal> terminal) = 0;

protected:
    CTEPServiceListener() = default;
    CTEPServiceListener(const CTEPServiceListener&) = delete;
    CTEPServiceListener& operator=(const CTEPServiceListener&) = delete;
};
}
