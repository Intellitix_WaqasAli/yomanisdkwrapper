//
//  ECTFinancialCounterInfo.h
//  EasyIntegrator
//
//  Created by Simon Salomons on 12/08/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__ECTFinancialCounterInfo__
#define __EasyIntegrator__ECTFinancialCounterInfo__

#include <string>
#include <vector>
#include <memory>
#include <boost/optional.hpp>
#include "easyctepdefs.h"

namespace ect {

  /**
   @brief
   Enum used to identify which service is linked to a financial counter.
   */

  enum class ECTFinancialCounterInfoAccountedServices {

#ifdef __APPLE__
#pragma mark - Properties -
#endif

    /**
     linked to the Sale Service.
     */
    saleService,

    /**
     linked to the Cash Advance Service.
     */
    cacheAdvanceService,

    /**
     linked to the Reservation Service.
     */
    reservationService,

    /**
     linked to the Sale After Reservation Service.
     */
    saleAfterReservationService,

    /**
     linked to the Card Validity Check Service.
     */
    cardValidityCheckService,

    /**
     linked to the Refund Service.
     */
    refundService,

    /**
     linked to the Deferred Sale Service.
     */
    deferredSaleService,

    /**
     linked to the Extra on Sale Service.
     */
    extraOnSaleService,

    /**
     linked to the Cancellation Service.
     */
    cancellationService
  };



  /**
   @brief
   Contains the information about a financial counter, which can be retrevied with the \link ECT::ECTBalanceTransactionResult Balance Service transactions\endlink.
   */
  class LIBRARY_API ECTFinancialCounterInfo {

  public:
     ECTFinancialCounterInfo(const std::string& serializedData);


    /**
     Identifies the counter.

     The financial counters are the property of the owning acquirer and under its sole control. Therefore, their identification is also matter of the acquirer that is fully responsible to manage appropriately its own financial counters. So the meaning that attaches to the identification value is a proprietary matter for which each acquirer may have its own acceptation or interpretation. Nevertheless, the acquirer should document for the merchant the way it manages its financial counters and the use the merchant could make of it.

     @return the identifier
     */
    std::string identifier() const;

    /**
     @return The currency used for the counter cumulative total amount.
     */
    std::string currencyCode() const;

    /**
     @return Textual translation of the financial counter identifier, which can be used for the merchant consultation of the counters or for the counter information printing.
     */
    std::string name() const;

    /**
     @return The total number of transactions that were performed (and accepted) since the period opening date and until the period closing date (if it is already known) or until the current time.
     */
    int transactionsNumber() const;

    /**
     @return The total amount (expressed in terms of the currency defined in the financial counter currency sub-field) of the transactions performed (and accepted) since the period opening date and until the period closing date (if it is already known) or until the current time.
     */
    double cumulativeTotalAmount() const;

    /**
     @return The period number that was allocated by the acquirer.
     */
    std::string periodNumber() const;

    /**
     @return The card brand identifier is used to set which card brand is linked to this counter. If this property is nil or if its value is 0000, the counter is not linked to any particular card brand.
     */
    std::string cardBrandIdentifier() const;

    /**
     @return The card brand name.
     */
    std::string cardBrandName() const;

    /**
     Indicate that the counter is related to the so-identified terminal, while the value nil indicates that the counter is not related to a particular terminal.
     @return the terminal identifier
     */
    std::string terminalIdentifier() const;

    /**
     @return The opening date of the period.
     */
    boost::optional<tm> periodOpeningDate() const;

    /**
     @return The closing date of the period.
     */
    boost::optional<tm> periodClosingDate() const;

    /**
     Set the value of the linked services.
     */
    void setAccountedServicesBinayValue(const std::string);

    /**
     Give a textual description of the linked services.
     */
    std::string accountedServicesDescription() const;

    /**
     Determine if a service is linked to a financial counter.
     @param service the service to check
     @return true if the given service is linked, false otherwise.
     */
    bool isServiceLinked(ECTFinancialCounterInfoAccountedServices service) const;



  protected:


    ECTFinancialCounterInfo() = default;

    //! @cond
    friend class ECTMessageResponseBalanceService;
    //! @endcond

    /**
     This field identifies the counter.

     The financial counters are the property of the owning acquirer and under its sole control. Therefore, their identification is also matter of the acquirer that is fully responsible to manage appropriately its own financial counters. So the meaning that attaches to the identification value is a proprietary matter for which each acquirer may have its own acceptation or interpretation. Nevertheless, the acquirer should document for the merchant the way it manages its financial counters and the use the merchant could make of it.
     */
    std::string identifier_;

    /**
     The currency used for the counter cumulative total amount.
     */
    std::string currencyCode_;

    /**
     Textual translation of the financial counter identifier, which can be used for the merchant consultation of the counters or for the counter information printing.
     */
    std::string name_;

    /**
     The total number of transactions that were performed (and accepted) since the period opening date and until the period closing date (if it is already known) or until the current time.
     */
    int transactionsNumber_;

    /**
     The total amount (expressed in terms of the currency defined in the financial counter currency sub-field) of the transactions performed (and accepted) since the period opening date and until the period closing date (if it is already known) or until the current time.
     */
    double cumulativeTotalAmount_;

    /**
     The period number that was allocated by the acquirer.
     */
    std::string periodNumber_;

    /**
     The card brand identifier is used to set which card brand is linked to this counter. If this property is nil or if its value is 0000, the counter is not linked to any particular card brand.
     */
    std::string cardBrandIdentifier_;

    /**
     This property is used to indicate that the counter is related to the so-identified terminal, while the value nil indicates that the counter is not related to a particular terminal.
     */
    std::string terminalIdentifier_;

    /**
     The opening date of the period.
     */
    boost::optional<tm> periodOpeningDate_;

    /**
     The closing date of the period.
     */
    boost::optional<tm> periodClosingDate_;

    std::string accountedServicesBinayValue_;


  private:
    void linkService(ECTFinancialCounterInfoAccountedServices service);
  };


}

#endif /* defined(__EasyIntegrator__ECTFinancialCounterInfo__) */
