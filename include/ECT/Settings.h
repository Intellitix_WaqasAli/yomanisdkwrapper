#pragma once

#include <boost/optional.hpp>
#include <chrono>
#include <string>

namespace ect {

using Seconds = std::chrono::seconds;
using MilliSeconds = std::chrono::milliseconds;

struct Settings {
    Seconds cardRecognitionTimeout{ 45 };
    Seconds cardInfoTrailingRequestTimeout{ 30 };
    bool certificationModeEnabled{ false };
    std::string certificationFilePath;
    std::string certificationFileName;
    std::string currencyCode;
    boost::optional<Seconds> deliveryTimeout;
    Seconds displayResultTimeout{ 2 };
    boost::optional<MilliSeconds> serialHeartBeatInterval;
};
}
