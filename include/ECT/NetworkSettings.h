#pragma once

#include <vector>
#include <boost/asio.hpp>

namespace ect
{

struct NetworkSettings
{
    int listenPort_{0};
    std::vector<boost::asio::ip::address> listenIps_;
};

}
