//
//  EncryptedData.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 22/12/16.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__EncryptedData__
#define __EasyIntegrator__EncryptedData__

#include <stdio.h>
#include <string>
#include <vector>
#include "ECTEncryption.h"

namespace ect {
  
  /**
   * @brief Data used with the VerifiyPin methods.
   */
  class EncryptedData
  {
    
  public:
    
    /**
     * Constructor
     *
     * @param encryptedPin the encrypted PIN code. As a list of bytes.
     * @param keyIndex the index of the key used to encrypt the PIN code.
     * @param keySerialNumber the serial number of the key used to encrypt the PIN code. As a list of bytes.
     * @param encryptionData the data used to encrypt the PIN code. As a list of bytes.
     * @param algorithm the algorithm used to encrypt the PIN code.
     */
    EncryptedData(const std::vector<uint8_t>& encryptedPin, const int keyIndex, const std::vector<uint8_t>& keySerialNumber, const std::vector<uint8_t>& encryptionData, ECTEncryptionAlgorithm algorithm);

    /**
     * Constructor
     *
     * @param encryptedPin the encrypted PIN code. As a string. For example "01AF45" will be interpreted as the list of bytes {0x01, 0xAF, 0x45}
     * @param keyIndex the index of the key used to encrypt the PIN code.
     * @param keySerialNumber the serial number of the key used to encrypt the PIN code. As a string. For example "01AF45" will be interpreted as the list of bytes {0x01, 0xAF, 0x45}
     * @param encryptionData the data used to encrypt the PIN code. As a string. For example "01AF45" will be interpreted as the list of bytes {0x01, 0xAF, 0x45}
     * @param algorithm the algorithm used to encrypt the PIN code.
     */
    EncryptedData(const std::string encryptedPin, const int keyIndex, const std::string keySerialNumber, const std::string encryptionData, ECTEncryptionAlgorithm algorithm);

    std::string encryptedPin;
    int keyIndex;
    std::string keySerialNumber;
    std::string encryptionData;
    ECTEncryptionAlgorithm algorithm;
    
  };
  
}

#endif /* defined(__EasyIntegrator__EncryptedData__) */
