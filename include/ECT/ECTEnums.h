//
//  ECTEnums.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 22/03/16.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef ECTEnums_h
#define ECTEnums_h

enum ProductDeliveryBlockReturnValue { ProductDeliveryBlockReturnValueRejected = 0, ProductDeliveryBlockReturnValueAccepted = 1 };

#endif /* ECTEnums_h */
