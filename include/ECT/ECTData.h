//
//  ECTData.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 30/07/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__ECTData__
#define __EasyIntegrator__ECTData__

#include "easyctepdefs.h"
#include <initializer_list>
#include <memory>
#include <stdio.h>
#include <string>
#include <vector>

#ifdef __APPLE__
#pragma mark - Typedef -
#endif

typedef unsigned char byte;

namespace ect {

#ifdef __APPLE__
#pragma mark - ECTData class -
#endif

class LIBRARY_API ECTData {
#ifdef __APPLE__
#pragma mark - Public members
#endif
public:
#ifdef __APPLE__
#pragma mark - Constructors
#endif
    ECTData();
    ECTData(const std::uint8_t* bytes, int length);
    explicit ECTData(const std::vector<byte>& data);

    ECTData(const char* data);
    // For example, for a string containing "ABC",
    // the ECTData object will contain <414243>.

    static ECTData fromString(const char* data);

    bool operator==(ECTData& other);
    bool operator!=(ECTData& other);

#ifdef __APPLE__
#pragma mark - Data management
#endif
    size_t length() const;
    std::vector<byte> bytes() const;

    ECTData subdataWithRange(int origin, size_t length) const;

    void appendData(const ECTData& data);

    //! @cond
    /**
     Convert a ECTData containing values from 0 to 9 (0..9) into a long representing the numeric
     value in the ECTData.
     For example, a ECTData containing <1234> will be converted into a long containing the value 1234.
     This is the C-TEP Decimal digit format:
     The basic “decimal digit” type is indicated in the data dictionary with the letter ‘i’.
     The decimal digit values are in the range 0 through 9 and are coded on 4 bits (1 nibble) using the BCD format.
     The bytes of this ECTData object should be encoded with the i C-TEP format otherwise will return 0.
     */
    long long CTEPLongValue() const;
//! @endcond

#ifdef __APPLE__
#pragma mark - String representations
#endif
    std::string description() const;
    std::string ascii() const;

    // Returns a string representation of the data.
    std::string stringValue() const;

    // Transform an ECTData into a string that represents the data with each by separated by a space.
    // For example:
    // 01 12 00 A3 F1 91 5D
    std::string stringValueWithSpaces() const;

    void dump() const;

#ifdef __APPLE__
#pragma mark - Protected members
#endif
protected:
    std::vector<byte> data_;
};
}

#endif /* defined(__EasyIntegrator__ECTData__) */
