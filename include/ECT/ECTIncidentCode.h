//
//  ECTIncidentCode.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 10/08/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__ECTIncidentCode__
#define __EasyIntegrator__ECTIncidentCode__

#include <string>
#include "easyctepdefs.h"

namespace ect {

  /**
   @brief
   Represents a incident code that can be sent by the terminal when it receives a transaction.

   Used as extra information of the error of an ECTErrorCode object.

   @see IncidentCodes for the list of codes.
   */
  class LIBRARY_API ECTIncidentCode
  {

  public:
    /**
     The code of the incident
     */
    std::string code() const;


    /**
     Determines if this incident code reprensents an error code.
     @return true if the incident code represents an error, false otherwise.
     */
    bool isErrorCode() const;

    /**
     Determines if this incident code reprensents a success code.
     @return true if the incident code represents a success, false otherwise.
     */
    bool isSuccess() const;



    ECTIncidentCode(std::string code);
  protected:

    //! @cond
    friend class ECTMessageBodyResponse;
    friend class ECTMessageResponse;

    //! @endcond

    std::string code_;
  };

}




/**
 \addtogroup Error
 @{
 @defgroup IncidentCodes Incident codes
 @brief Incident codes sent by the terminal

 Here are the different incident codes that can be in the \link ECTError#incidentCode incidentCode property\endlink of the ECTError:

 \li \c 1802 : Unexpected message
 \li \c 1803 : Timeout expiration
 \li \c 1811 : Technical problem
 \li \c 1822 : Connection failure
 \li \c 1823 : Reserved
 \li \c 2000 : Unknown acquirer identifier
 \li \c 2100 : Action code not supported
 \li \c 2625 : Corrupted message
 \li \c 2628 : External Equipment Cancellation
 \li \c 2629 : User cancellation
 \li \c 2630 : Device cancellation
 \li \c 2631 : Host cancellation
 \li \c 2632 : Host error
 \li \c 2633 : Operation already performed
 \li \c 2634 : Operation busy
 \li \c 2635 : Operation not performed
 \li \c 2800 : Doesn’t exist
 \li \c 2802 : Not allowed
 \li \c 2806 : Bad signature
 \li \c 2807 : Conditional field missing
 \li \c 2808 : Not found
 \li \c 2809 : Dependency not found
 \li \c 2810 : Bad value
 \li \c 2811 : Bad sequence
 \li \c 2812 : Device attachment
 \li \c 2813 : Unexpected field
 \li \c 3100 : Chip card expected
 \li \c 3101 : Card not well read
 \li \c 3102 : Condition of use not satisfied
 \li \c 4000 : Purse technical problem
 \li \c 4001 : Purse host identifier invalid
 \li \c 4002 : Purse SDA certificate error
 \li \c 4003 : Purse extended SDA certificate error
 \li \c 4004 : Purse in red list
 \li \c 4005 : Purse is locked for credit
 \li \c 4006 : Purse is locked for debit
 \li \c 4007 : Purse expired
 \li \c 4008 : Purse state error
 \li \c 4009 : Purse recovery error
 \li \c 4010 : Purse key identifier error
 \li \c 4011 : Purse balance too large
 \li \c 4012 : Insufficient purse balance
 \li \c 4100 : No purse in reader and time out expired
 \li \c 4101 : Time-out on fallback card reading
 \li \c 4102 : Problem linked to card
 \li \c 4103 : Card information not available
 \li \c 4200 : Entered amount invalid
 \li \c 4201 : Double operation
 \li \c 4202 : Invalid currency
 \li \c 4203 : Amount higher than authorized amount
 \li \c 4204 : Floor limit exceeded in EMV mode
 \li \c 4205 : Transaction refused by the terminal in EMV mode
 \li \c 4206 : Transaction refused by the card in EMV mode
 \li \c 4207 : Product not available
 \li \c 4300 : Service (already) activated
 \li \c 4301 : Service (already) deactivated
 \li \c 4302 : Maximal transaction number per (calendar) month reached
 \li \c 4303 : Maximal uncollected journals number reached
 \li \c 4304 : Service activation not supported
 \li \c 4305 : Maximum transaction records reached
 \li \c 4306 : Maximum service activation number reached
 \li \c 6003 : Paper jam
 \li \c 6004 : Remove previous ticket
 \li \c 6005 : No paper
 \li \c 6006 : Low paper
 \li \c 6008 : Printer specific
 \li \c 7806 : Product not allowed
 \li \c 7808 : Bad pump number
 \li \c 7816 : Incorrect pump session number
 \li \c 7817 : Transaction amount null
 \li \c 7818 : Transaction amount null and quantity null
 \li \c 7819 : Pump unhooked time-out expiration
 \li \c 9002 : No key fault
 \li \c 9003 : Cryptographic fault
 \li \c 9004 : No PIN fault
 \li \c 9005 : Bad MAC
 \li \c 9006 : Bad MDC

 @}
 @}
 */


#endif /* defined(__EasyIntegrator__ECTIncidentCode__) */
