//
//  ECTError.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 4/08/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__ECTError__
#define __EasyIntegrator__ECTError__

#include <string>
#include <map>
#include <memory>
#include <boost/optional.hpp>
#include "ECTIncidentCode.h"
#include "ECTRejectionCode.h"
#include "easyctepdefs.h"

namespace ect {

  /** @defgroup Error ECTError related modules
   *  @{
   *  @}
   */
  
  /** \addtogroup Error
   *  @{
   */
  
  /**  @defgroup ErrorCodes Error codes
   *  The error codes give the reason of the error.
   *
   *  For example, you can get the reason like this:
   *  @code
   *  if (error.code == ECTErrorCode_No_WIFI) {
   *    // There is no Wif connection available.
   *  }
   *  @endcode
   *
   *  @brief Error codes used with the property \link ECTError#code code \endlink in ECTError.
   *  @{
   */
  
#ifdef __APPLE__
#pragma mark - Error codes -
#endif

  enum class LIBRARY_API ECTErrorCode {
    
    /**
     The request was rejected.
     <br/>
     See the causeCode property for more information.
     */
    Rejected = 1,
    
    /**
     There was a technical issue while doing the request.
     <br/>
     See the causeCode property for more information.
     */
    Technical_Issue = 2,
    
    /**
     No Wifi connection available.
     */
    No_WIFI = 3,
    
    /**
     The time out for the request has been reached.
     */
    Timeout = 5
  };
  
  
  /** @} */ // end of Error codes
  /** @} */ // end of add to group

  
  
#ifdef __APPLE__
#pragma mark - Error cause codes -
#endif
  
  /** \addtogroup Error
   *  @{
   */
  
  /** @defgroup ErrorCauseCodes Error cause codes
   *  Some ECTError codes need more information.
   *  Error cause codes add this extra information to the ECTError objects.
   *
   *  For example, you can get the cause like this:
   *  @code
   *  if (error.code == ECTErrorCode_Technical_Issue) {
   *    // There is technical issue, let's find the cause.
   *    if (*error.causeCode == ECTErrorCauseCode_UnableToStartServer) {
   *      // The cause is that the server cannot be started.
   *    }
   *  }
   *  @endcode
   *
   *  @brief Error cause codes used with the property \link ECTError#causeCode causeCode \endlink in ECTError.
   *  @{
   */
  
  enum class LIBRARY_API ECTErrorCauseCode {

    /**
     The server cannot be started.
     */
    UnableToStartServer = 1,
    
    /**
     The server received an invalid connection advice message from the terminal.
     */
    InvalidConnectionAdvice = 11,
    
    /**
     The server has no connection with the terminal.
     */
    NoConnectionWithTerminal = 12,
    
    /**
     The server has already a connection with the terminal.
     */
    ExistingConnectionWithTerminal = 13,
    
    /**
     A transaction is already in progress.
     */
    TransactionAlreadyRunning = 14,
    
    /**
     The service received an invalid request.
     */
    InvalidRequest = 15,
    
    /**
     The service received an invalid response.
     */
    InvalidResponse = 16,
    
    /**
     The terminal identifier is not in the connected terminals list.
     */
    UnknownTerminalIdentifier = 17,
    
    /**
     There was an issue with the network connection.
     */
    NetworkError = 18,
    
    /**
     The terminal sent an incident code.
     <br/>
     See the incidentCode property of the error to get more information.
     */
    IncidentCode = 19,
    
    /**
     The terminal sent an rejection code.
     <br/>
     See the rejectionCode property of the error to get more information.
     */
    RejectionCode = 20,
    
    /**
     The running transaction has been reset by the reset command of the ECTService.
     */
    ResetTransaction = 30,
    
    /**
     The request has been reset by the ECR
     */
    ResetByECR = 31, 
    
    /**
     The specified acquirer identifier in the request has an invalid format.
     <br/>
     The valid format is a 8 digits string, for example:
     @code
     @"12345678"
     @endcode
     */
    InvalidAcquirerIdentifier = 50,
    
    /**
     The specified data has an invalid format.
     */
    InvalidData = 51,
    
    /**
     The specified currency in the request has an invalid format.
     <br/>
     The valid format is a the iso code of the currency, for example:
     @code
     @"EUR"
     @endcode
     */
    RetailService_InvalidCurrencyCode = 110,
    
    /**
     The specified amount in the Retail Service request has an invalid format.
     <br/>
     */
    RetailService_InvalidAmount = 112,
    
    /**
     The specified merchant reference in the Retail Service request is invalid.
     <br/>
     */
    RetailService_InvalidMerchantReference = 114,
    
    /**
     The specified authorized code in the Retail Service request is invalid.
     <br/>
     */
    RetailService_InvalidAuthorizationCode = 115,
    
    
    /**
     The specified discretionary data in the Retail Service request are invalid.
     <br/>
     */
    RetailService_InvalidDiscretionaryData = 116,
    
    /**
     The specified ticket text in the Ticket Service request are invalid.
     <br/>
     */
    RetailService_InvalidTicketText = 117,
    
    /**
     The cashback amount is not specified.
     <br/>
     */
    RetailService_CashbackNotSpecified = 118,
    
    /**
     The cashback transaction is not allowed for the current currency.
     <br/>
     */
    RetailService_CashbackNotAllowedForCurrency = 119,
    
    /**
     The specified discretionary data in the Retail Service request are invalid.
     <br/>
     */
    RetailService_InvalidProprietaryData = 120,


    /**
     The user pressed the Stop key instead of the Ok key.
     <br/>
     */
    MMIService_StopKeyPressed = 200,
    
    /**
     The pin code is invalid
     */
    MMIService_InvalidPinCode = 201
  };
  
  /** @} */ // end of Error codes
  /** @} */ // end of add to group
  
  
  
  
  
  
#ifdef __APPLE__
#pragma mark - ECTError class -
#endif
  
  /**
   * @brief
   * Used to encapsulate an error used by the framework.
   *
   * More information on the different codes:
   * - \link ErrorCodes Error codes\endlink
   * - \link ErrorCauseCodes Error cause codes\endlink
   * - \link IncidentCodes Incident codes\endlink
   * - \link RejectionCodes Rejection codes\endlink
   * - \link ErrorExtraInfoKeys extraInformationData\endlink dictionary keys
   */
  class LIBRARY_API ECTError {

#ifdef __APPLE__
#pragma mark - Public members -
#endif
  public:
    
#ifdef __APPLE__
#pragma mark - Properties -
#endif

    /**
     The error code.
     
     This is the primary error information.
     
     @see \link ErrorCodes Error codes\endlink
     */
    ECTErrorCode code() const;
    
    /**
     The error cause code.
     
     This is the secondary error information that gives more detail.
     
     @see \link ErrorCauseCodes Error cause codes\endlink
     */
    boost::optional<ECTErrorCauseCode> causeCode() const;

    /**
     The error extra information data.
     
     @see \link ErrorExtraInfoKeys extraInformationData dictionary keys\endlink for more information.
     */
    std::map<std::string, std::string> extraInformationData() const;
    
    /**
     The incident code if it exists.
     
     @see \link IncidentCodes Incident codes\endlink for more information.
     */
    boost::optional<ECTIncidentCode> incidentCode() const;

    /**
     The rejection code if it exists.
     
     @see \link RejectionCodes Rejection codes\endlink for more information.
     */
    boost::optional<ECTRejectionCode> rejectionCode() const;


#ifdef __APPLE__
#pragma mark - Object lifecycle -
#endif

    /**
     Initialize an ECTError with the given error code.
     @param code the error code.
     @return an instance of ECTError representing the error code.
     */
    ECTError(ECTErrorCode code);
    
    /**
     Initialize an ECTError with the given error code and error cause code.
     @param code the error code.
     @param causeCode the code of the cause of the error
     @return an instance of ECTError representing the error code.
     */
    ECTError(ECTErrorCode code, ECTErrorCauseCode causeCode);
    
    /**
     Initialize an ECTError with the given error code.
     @param code the error code.
     @param extraInformation the extra data information dictionary
     @return an instance of ECTError representing the error code.
     */
    ECTError(ECTErrorCode code, std::map<std::string, std::string> extraInformation);
    
    /**
     Initialize an ECTError with the given error code and error cause code.
     @param code the error code.
     @param causeCode the code of the cause of the error
     @param extraInformation the extra data information dictionary
     @return an instance of ECTError representing the error code.
     */
    ECTError(ECTErrorCode code, ECTErrorCauseCode causeCode, std::map<std::string, std::string> extraInformation);
    
    /**
     Initialize an ECTError with the given error code and error cause code.
     @param code the error code.
     @param causeCode the code of the cause of the error
     @param incidentCode the incident code.
     @param extraInformation the extra data information dictionary
     @return an instance of ECTError representing the error code.
     */
    ECTError(ECTErrorCode code, ECTErrorCauseCode causeCode, ECTIncidentCode &incidentCode, std::map<std::string, std::string> extraInformation);

    /**
     Initialize an ECTError with the given error code and error cause code.
     @param code the error code.
     @param incidentCode the incident code.
     @param extraInformation the extra data information dictionary
     @return an instance of ECTError representing the error code.
     */
    ECTError(ECTErrorCode code, ECTIncidentCode &incidentCode, std::map<std::string, std::string> extraInformation);

    /**
     Initialize an ECTError with the given Incident Code.
     
     Incident codes are error codes from the CTEP protocol.
     
     @param incidentCode the incident code.
     @return an instance of ECTError representing the error code.
     */
    ECTError(const ECTIncidentCode &incidentCode);

    /**
     Initialize an ECTError with the given Incident Code.
     
     Incident codes are error codes from the CTEP protocol.
     
     @param incidentCode the incident code.
     @param extraInformation the extra data information dictionary
     @return an instance of ECTError representing the error code.
     */
    ECTError(const ECTIncidentCode &incidentCode, std::map<std::string, std::string> extraInformation);

    /**
     Initialize an ECTError with the given Rejection Code.
     
     Rejection codes are error codes from the CTEP protocol.
     
     @param rejectionCode the extra data information
     @return an instance of ECTError representing the error code.
     */
    ECTError(const ECTRejectionCode &rejectionCode);

    /**
     Initialize an ECTError with the given Rejection Code.
     
     Rejection codes are error codes from the CTEP protocol.
     
     @param rejectionCode the extra data information
     @param extraInformation the extra data information dictionary
     @return an instance of ECTError representing the error code.
     */
    ECTError(const ECTRejectionCode &rejectionCode, std::map<std::string, std::string> extraInformation);
    

    std::string description() const;

    

#ifdef __APPLE__
#pragma mark - Private members -
#endif
  private:
    
    //! @cond
    friend class ECTService;
    //! @endcond

    ECTErrorCode code_;
    boost::optional<ECTErrorCauseCode> causeCode_;
    std::map<std::string, std::string> extraInformation_;
    boost::optional<ECTIncidentCode> incidentCode_;
    boost::optional<ECTRejectionCode> rejectionCode_;
  };
}


#endif /* defined(__EasyIntegrator__ECTError__) */