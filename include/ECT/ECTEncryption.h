//
//  ECTEncryption.h
//  Project
//
//  Created by Marc Jordant on 15/09/16.
//
//

#ifndef ECTEncryption_h
#define ECTEncryption_h

namespace ect {
  
  /** @defgroup Encryption ECTEncryption related modules
   *  @{
   *  @}
   */
  
  /** \addtogroup Encryption
   *  @{
   */
  
  /**  @defgroup ErrorCodes encryption algorithms
   *  The available algorithms for encryption.
   *
   *  @brief
   *  @{
   */
  
#ifdef __APPLE__
#pragma mark - ECTEncryptionAlgorithm -
#endif
  
  enum class ECTEncryptionAlgorithm {
    
    /**
     None.
     */
    None = 0,
    
    /**
     AES.
     */
    AES_CTR = 1,
    
    };
    
    /**
     *  @}
     *  @}
     */
    
}

#endif /* ECTEncryption_h */
