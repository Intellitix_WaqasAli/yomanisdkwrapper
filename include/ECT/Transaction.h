#pragma once

#include "easyctepdefs.h"
#include <atomic>
#include <functional>
#include <memory>
#include <string>

namespace ect {

class TransactionContext;

/*!
 * \brief The Transaction class represents a interface for making transaction
 *  Every transaction must implement this interface
 */
class LIBRARY_API Transaction {
public:
    virtual ~Transaction() {}

    /*!
     * \internal
     * \brief move Moves a transaction. The original object becomes invalid after this operation (similar to std::move)
     * \return The transaction object
     */
    virtual std::unique_ptr<Transaction> move() = 0;

    /*!
     * \internal
     * \brief serialize Serialize the transaction information to a byte stream
     * \return The serialized data
     */
    std::string onBeginTransaction(TransactionContext&);

    /*!
     * \internal
     * \brief notifyResult Accepts the incoming data, convert it to a suitable C++ object and forwards it to an another destination
     * \param result
     */
    void onEndTransaction(const std::string& result);

    /*!
     * \internal
     * \brief isFinished Checks if the transaction is finished. A transaction finishes if the result is received?
     * \return True if the transaction result was received, false otherwise.
     */
    bool isFinished() const;

protected:
    Transaction();
    Transaction(const Transaction&) = delete;
    Transaction& operator=(const Transaction&) = delete;

    Transaction(Transaction&&) = default;
    Transaction& operator=(Transaction&&) = default;

    virtual std::string beginTransaction(TransactionContext&) = 0;
    virtual bool endTransaction(const std::string&, std::function<void(const std::string&)>) = 0;


private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};
}
