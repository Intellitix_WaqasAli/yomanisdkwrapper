//  FreeText.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 09/01/17.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__FreeText__
#define __EasyIntegrator__FreeText__

#include <stdio.h>
#include <string>

namespace ect {
  
  /**
   * @brief Data used with the VerifiyPin methods.
   */
  class FreeText
  {
  public:
    FreeText(std::string text_1, std::string text_2, std::string text_3, std::string text_4);
    
    std::string text_1() const;
    std::string text_2() const;
    std::string text_3() const;
    std::string text_4() const;

  
  private:
    std::string text_1_;
    std::string text_2_;
    std::string text_3_;
    std::string text_4_;
  };
  
}

#endif /* defined(__EasyIntegrator__FreeText__) */
