#pragma once

#include <chrono>
#include <string>
#include <cstdint>
#include "easyctepdefs.h"

namespace ect
{

class LIBRARY_API Amount
{
public:
    /*!
     * \brief Amount
     * \param amount String containg a decimal number (e.g.: 6,433)
     *        comma and dot representation is accepted
     */
    explicit Amount(const std::string& amount);

    double value() const;

    std::int64_t integralPart() const;

    /*!
     * \brief decimalPart (trailing 0's are added to have at least 2 digits)
     * \return Return the decimal part with trailing 0's added
     */
    std::int64_t decimalPart() const;

    std::string toString() const;

private:
    std::int64_t integral_;
    std::int64_t decimal_;
    double amount_;
};

}
