#pragma once

#include <string>
#include <vector>
#include <time.h>
#include "easyctepdefs.h"
#include <memory>

namespace ect {

/*!
 * \brief The erase mode used when extracting files.
 */
enum CTEPRecoveryFileEraseMode
{
    /**
     * The extracted files will NOT be deleted.
     */
    ERASE_NONE = 0,

    /**
     * The extracted files will be deleted.
     */
    ERASE_AFTER_RECOVERY = 1
};


/*!
 * \brief Represents a file generated with the recovery mode.
 */
class LIBRARY_API CTEPRecoveryFile {
public:

    /**
     * \brief CTEPRecoveryFile default constructor
     */
    CTEPRecoveryFile();

    /**
     * \brief CTEPRecoveryFile constructor to fill in all the members
     */
    CTEPRecoveryFile(std::string acquirerId,
                     std::string terminalId,
                     std::string schemeId,
                     std::string batchId,
                     tm closingDate,
                     std::string content,
                     std::string fileName);

    /**
     * \brief ~CTEPRecoveryFile
     */
    virtual ~CTEPRecoveryFile() {}

    /*!
     * \brief Get the acquirer identifier related to this recovery file.
     * \return The acquirer identifier related to this recovery file.
     */
    virtual std::string acquirerId() const;

    /*!
     * \brief Get the terminal identifier related to this recovery file.
     * \return The terminal identifier related to this recovery file.
     */
    virtual std::string terminalId() const;

    /*!
     * \brief Get the scheme identifier related to this recovery file.
     * \return The scheme identifier related to this recovery file.
     */
    virtual std::string schemeId() const;

    /*!
     * \brief Get the batch identifier related to this recovery file.
     * \return The batch identifier related to this recovery file.
     */
    virtual std::string batchId() const;

    /*!
     * \brief Get the closing date of this recovery file.
     * \return The closing date of this recovery file.
     */
    virtual tm closingDate() const;

    /*!
     * \brief Get the content of the file in the required format.
     * \return The content of the file in the required format.
     */
    virtual std::string content() const;

    /*!
     * \brief Get the file name of this recovery file.
     * \return The file name of this recovery file.
     */
    virtual std::string fileName() const;

protected:

    std::string acquirerId_;
    std::string terminalId_;
    std::string schemeId_;
    std::string batchId_;
    tm closingDate_;
    std::string content_;
    std::string fileName_;
};

/*!
 * \brief Returns an iterable list containing the available closed recovery files for all TID
 * \return an iterable list containing the available closed recovery files for all TID
 */
std::vector<std::shared_ptr<CTEPRecoveryFile>> getRecoveryFiles();

/*!
 * \brief Returns an iterable list containing the available closed recovery files for the given TID.
 * \param tid the TID of the terminal for which the recovery files are listed.
 * \return an iterable list containing the available closed recovery files for the given TID.
 */
std::vector<std::shared_ptr<CTEPRecoveryFile>> getRecoveryFiles(const std::string& tid);

/*!
 * \brief extract the closed recovery files for the all TID into the directory located at destinationPath.
 * \param destinationPath the path and name of the destination directory.
 * \param eraseMode the erase mode.
 */
bool extractRecoveryFiles(const std::string& destinationPath, CTEPRecoveryFileEraseMode eraseMode);

/*!
 * \brief extract the closed recovery files for the given TID into the directory located at destinationPath.
 * \param tid the TID of the terminal for which the recovery files are extracted.
 * \param destinationPath the path and name of the destination directory.
 * \param eraseMode the erase mode.
 */
bool extractRecoveryFiles(const std::string& tid, const std::string& destinationPath, CTEPRecoveryFileEraseMode eraseMode);

}
