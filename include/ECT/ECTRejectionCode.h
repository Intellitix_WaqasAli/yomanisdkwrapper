//
//  ECTRejectionCode.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 10/08/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__ECTRejectionCode__
#define __EasyIntegrator__ECTRejectionCode__

#include <string>
#include "easyctepdefs.h"

namespace ect {

  /**
   @brief
   Represents a rejection code that can be sent by the terminal when it receives a transaction.

   Used as extra information of the error of an ECTErrorCode object.

   @see RejectionCodes for the list of codes.
   */
  class LIBRARY_API ECTRejectionCode
  {
  public:
    /**
     @brief
     The code of the rejection

     @see RejectionCodes for the list of codes.
     */
    std::string code() const;

    ECTRejectionCode(std::string code);

  protected:
    //! @cond

    friend class ECTMessageBodyResponse;
    friend class ECTMessageResponse;


    //! @endcond


    std::string code_;
  };

}

/**
 \addtogroup Error
 @{
 @defgroup RejectionCodes Rejection codes
 @brief Rejection codes sent by the terminal

 Here are the different rejection codes that can be in the \link ECTError#rejectionCode rejectionCode property\endlink of the ECTError:

 \li \c 0001 : Service unavailable
 \li \c 0002 : Invalid message
 \li \c 0003 : Message not supported
 \li \c 0004 : Service not found

 @}
 @}
 */

#endif /* defined(__EasyIntegrator__ECTRejectionCode__) */
