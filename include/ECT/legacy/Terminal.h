#pragma once

#include <boost/asio.hpp>
#include <string>

namespace ect {

class Transaction;

/*!
 To make a transaction you need to send transaction commands to terminal
 \par Example 1: Make a simple _Sale_
 To make a sale transaction, use the following code:
 @code
 // C++14
 terminal->send(SaleTransaction(SaleAction::Sale, Amount("19.99"), "Merchant Reference", [](const auto& result) {
                                        // do something with the result
                                    }

 // C++11
 terminal->send(SaleTransaction(SaleAction::Sale, Amount("19.99"), "Merchant Reference", [](const SaleResult& result) {
                                        // do something with the result
                                    }
 @endcode
 */

/*!
 \par Example 2: Cancel a retail transaction:
 @code
 // C++14
 terminal->send(CancelRetailTransaction([](const auto& result) {
                                    // do something with the result
                               }));

 // C++11
 terminal->send(CancelRetailTransaction([](const CancelRetailResult& result) {
                                    // do something with the result
                               }));
                                    }
 @endcode
 */
class Terminal {
public:
    virtual ~Terminal() {}
    /*!
     * \brief Get the terminal identifier
     * \return The terminal identifier
     */
    virtual std::string terminalId() const = 0;
    /*!
     * \brief Get the device type of the terminal
     * \return The device type
     */
    virtual std::string deviceType() const = 0;
    /*!
     * \brief Get the model name of the terminal
     * \return The model name
     */
    virtual std::string model() const = 0;
    /*!
     * \brief Get the serial number of the terminal
     * \return The serial number
     */
    virtual std::string serialNumber() const = 0;
    /*!
     * \brief Get the software version of the terminal
     * \return  The software version
     */
    virtual std::string softwareVersion() const = 0;
    /*!
     * \brief Get the EMV kernel version of the terminal
     * \return The EMV kernel version
     */
    virtual std::string EMVKernelVersion() const = 0;
    /*!
     * \brief Get the tcp/ip addres of the terminal
     * \return The IP address
     */
    virtual boost::asio::ip::address ipAddress() const = 0;
    /*!
     * \brief Get the tcp/ip port of the terminal
     * \return The IP port
     */
    virtual int port() const = 0;
    /*!
     * \brief Is the terminal connected
     * \return True if the terminal is connected. False otherwise
     */
    virtual bool connected() const = 0;
    /*!
     * \brief Make a transaction by sending a transaction object
     * \param transaction The transaction object
     */
    virtual void send(Transaction&& transaction) = 0;
    /*!
     * \brief Reset any open transaction
     */
    virtual void resetTransaction() = 0;

protected:
    Terminal() = default;
    Terminal(const Terminal&) = delete;
    Terminal& operator=(const Terminal&) = delete;
};
}
