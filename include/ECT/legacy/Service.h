#pragma once

#include <boost/asio.hpp>
#include <string>
#include <vector>

namespace ect {

struct Settings;

/*!
 * \brief The Service interface is used to start and stop the service gateway and to configure it
 */
class Service {
public:
    virtual ~Service() {}

    /*!
     * \brief Start listening for connected terminals
     * \param serviceListenPort The service ip listen port
     */
    virtual void startService(int serviceListenPort) = 0;

    /*!
     * \brief Stop the service
     */
    virtual void stopService() = 0;

    /*!
     * \brief Get a list of local binded ip address
     * \return A list of ip addressess listening on
     */
    virtual std::vector<boost::asio::ip::address> listenIps() = 0;

    /*!
     * \brief Get the service listen port
     * \return The service listen port
     */
    virtual int listenPort() = 0;

    /*!
     * \brief Get the current service settings
     * \return The current service settings
     */
    virtual Settings settings() const = 0;

    /*!
     * \brief Update the current service settings
     * \param settings The new service settings
     */
    virtual void updateSettings(const Settings& settings) = 0;

    /**
     * \brief Enable/disable the notifications of the terminal.
     * \param terminalIdentifier the identifier of the terminal on which make the transaction
     * \param enable when true the notifications will be sent, when false the notifications will not be sent
     */
    virtual void enableNotifications(std::string terminalIdentifier, bool enable) = 0;

protected:
    Service() = default;
    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;
};
}
