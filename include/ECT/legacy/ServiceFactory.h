#pragma once

#include <memory>
#include "easyctepdefs.h"

namespace ect
{

class ServiceListener;
class Service;

/*!
 * \brief MakeService
 *        This factory creates your service instance
 * \param listener Your ServiceListener instance
 * \return A new Service instance, nullptr if failed
 */
LIBRARY_API std::shared_ptr<Service>  MakeService(ServiceListener& listener);

}
