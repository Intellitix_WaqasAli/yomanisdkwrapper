#ifndef CARDBRANDIDENTIFIER_H
#define CARDBRANDIDENTIFIER_H

namespace ect {

  enum CardBrandIdentifier
  {
       BancontactMisterCash = 1,
       Bancontact = 1 << 1,
       Maestro = 1 << 2,
       MasterCard = 1 << 3,
       Visa = 1 << 4,
       VisaElectron = 1 << 5,
       VPAY = 1 << 6,
       AMEX = 1 << 7,
       WLCompanyCards = 1 << 8,
       WLCompanyCardsInternational = 1 << 9,
       CartesBancaires = 1 << 10,
       DinersDiscover = 1 << 11,
       JCB = 1 << 12,
       UnionPay = 1 << 13,
       Monizze = 1 << 14,
       CMFC = 1 << 15,
       BasicCard = 1 << 16,
       BuyWay = 1 << 17,
       PASS = 1 << 18,
       SodexoCard = 1 << 19,
       Edenred = 1 << 20,
       CCVCard = 1 << 21,
       Travelcard = 1 << 22,
       CompanyCards = 1 << 23,
       WorldlineWL = 1 << 24,
       EquensWL = 1 << 25,
       Yourgift = 1 << 26,
       Giftcard = 1 << 27,
       KADOZ = 1 << 28,
       RES = 1 << 29,
       GoodPay = 1 << 30,
       Payconiq = 1 << 31
    };

}

#endif // CARDBRANDIDENTIFIER_H
