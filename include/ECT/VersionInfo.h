#pragma once

#include <string>

namespace ect
{

/*!
 * \brief getLibraryVersion
 * \return The library version e.g.: 1.0.2 (Format: http://semver.org/)
 */
std::string getLibraryVersion();

}
