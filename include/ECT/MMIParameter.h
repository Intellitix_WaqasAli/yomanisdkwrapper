#pragma once

#include <string>
#include <boost/optional.hpp>
#include "easyctepdefs.h"
#include "EncryptedData.h"
#include "FreeText.h"


namespace ect {
  /**
   @brief
   Contains the parameters for a MMI request.
  
   Used with the MMIRequest transaction.
   */
  class LIBRARY_API MMIParameter
  {
    
  public:
    
    MMIParameter();
    
    /**
     The display timeout (of the screen)
     */
    int displayTimeout;
    
    /**
     The identifier of the first text.
     */
    boost::optional<int> firstTextIdentifier;

    /**
     The language code of the first text.
     
     For example (for English): en
     */
    boost::optional<std::string> firstTextLanguage;
    
    
    /**
     The identifier of the second text.
     */
    boost::optional<int> secondTextIdentifier;

    /**
     The language code of the second text.
     
     For example (for English): en
     */
    boost::optional<std::string> secondTextLanguage;

    /**
     The pin code to check
     */
    boost::optional<EncryptedData> encryptedPin;


    /**
     A free text.
     
     Maximum 20 characters
     */
    boost::optional<FreeText> freeText;

  };
}

