//
//  ECTTerminalEvent.hpp
//  EasyIntegrator
//
//  Created by Marc Jordant on 23/09/16.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__ECTTerminalEvent__
#define __EasyIntegrator__ECTTerminalEvent__

#include <string>
#include <map>
#include <memory>
#include <boost/optional.hpp>
#include "easyctepdefs.h"

namespace ect {
  
  /** @defgroup TerminalEvent Terminal events related modules
   *  @{
   *  @}
   */
  
  /** \addtogroup TerminalEvent
   *  @{
   */
  
  /**  @defgroup ECTTerminalEvents Events of the terminal
   *
   *  @brief The events the terminal can send
   *  @{
   */
  
#ifdef __APPLE__
#pragma mark - ECTTerminalEvents -
#endif
  
  enum class LIBRARY_API ECTTerminalEvents {
    ECTTerminalEventsTerminalIdle,
    ECTTerminalEventsTerminalMaintenance,
    ECTTerminalEventsTerminalOutOfOrder,
    ECTTerminalEventsTerminalBusy,
    
    ECTTerminalEventsCardReaderIdle,
    ECTTerminalEventsCardReaderOutOfOrder,
    
    ECTTerminalEventsPINEntry,
    ECTTerminalEventsAdditionalDataEntry,
    
    ECTTerminalEventsInsertCard,
    ECTTerminalEventsSwipeCard,
    ECTTerminalEventsRemoveCard,
    
    ECTTerminalEventsAwaitingNextCard,
    ECTTerminalEventsSlaveCard,
    
    ECTTerminalEventsCardInserted,
    ECTTerminalEventsCardRemoved,
    ECTTerminalEventsBadReading,
    
    ECTTerminalEventsAmountConfirmation,
    ECTTerminalEventsConfigurationChanged,
    ECTTerminalEventsTransactionStarted,
    ECTTerminalEventsCurrencySelection
  };
  
    
    /** @} */ // end of ECTTerminalEvents
    /** @} */ // end of add to group
    
  
      
#ifdef __APPLE__
#pragma mark - ECTTerminalEvent class -
#endif
      
      /**
       * @brief
       * Represents an event the Synchronization Service of the terminal sent.
       */
      class LIBRARY_API ECTTerminalEvent {
        
#ifdef __APPLE__
#pragma mark - Public members -
#endif
      public:
        
#ifdef __APPLE__
#pragma mark - Properties -
#endif
        
        // Returns the identifier of the terminal of this event
        std::string terminalIdentifier;

        
        // Returns true if the specified event type is part of the events of this advice.
        bool isEvent(ECTTerminalEvents event) const;
        
        // Returns a basic text description of the contained events
        std::string eventsString() const;
        
        std::string description() const;
        
        std::string value;
        
#ifdef __APPLE__
#pragma mark - Private members -
#endif
      private:
        
        std::string binaryString() const;

      };
    }
    
    
#endif /* defined(__EasyIntegrator__ECTTerminalEvent__) */
