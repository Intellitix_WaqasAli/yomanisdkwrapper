//
//  ECTSaleSystemActionIdentifier.h
//  EasyIntegrator
//
//  Created by Marc Jordant on 4/08/15.
//  Copyright (c) 2015 PegusApps. All rights reserved.
//

#ifndef __EasyIntegrator__ECTSaleSystemActionIdentifier__
#define __EasyIntegrator__ECTSaleSystemActionIdentifier__

#include "ECTData.h"


namespace ect {
  
  /**
   @brief
   Represents a unique transaction identifier for the Sale System transactions.
   
   Used in the Retail Sale transactions.
   */
  class LIBRARY_API ECTSaleSystemActionIdentifier
  {
  public:
    ECTData data;
    
    /**
     Initialize a new ECTSaleSystemActionIdentifier object with the default identifier of 00 00 00 00.
     */
    ECTSaleSystemActionIdentifier();

    /**
     Initialize a new ECTSaleSystemActionIdentifier object with a version represented by the given data.
     
     @param data the identifier as a ECTData. The data object must contain 4 bytes.
     */
    ECTSaleSystemActionIdentifier(const ECTData data);

    /**
     Initialize a new ECTSaleSystemActionIdentifier object with a identifier represented by the 4 given bytes.
     
     @param byte1 the first byte of the identifier
     @param byte2 the second byte of the identifier
     @param byte3 the third byte of the identifier
     @param byte4 the fourth byte of the identifier
     */
    ECTSaleSystemActionIdentifier(byte byte1, byte byte2, byte byte3, byte byte4);

    /**
     Initialize a new ECTSaleSystemActionIdentifier object with a value represented by the given data.

     @param data the identifier as a long. The value cannot be higher than 4294967295 (maximum that can be represented with 4 bytes).
     */
    ECTSaleSystemActionIdentifier(unsigned long value);

    /**
     @return the ECTSaleSystemActionIdentifier value as a long
     */
    unsigned long value();
  };
}

#endif /* defined(__EasyIntegrator__ECTSaleSystemActionIdentifier__) */
