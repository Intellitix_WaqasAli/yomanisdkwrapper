#pragma once

/*! \mainpage Easy Integrator C++ Library
 *
 * \section intro_sec Introduction
 *
 *
 * The Easy Integrator Library provides a solution to make integration between sales applications and WL (Worldline) terminals easier.
 *
 * Functionalities:
 * - The integration kit on mobile devices serves as gateway between mobile applications and the terminal, that hides complexity (encoding, decoding of messages, communication layer dependencies, OS dependencies, etc).
 * - Provides an easier and logical functional interface.
 * - Financial transactions
 * - Easy and secure connection setup between mobile device and terminal
 * - Easy validation of the integration made by the integrator by implementing a certification mode in the SDK.
 *
 \section quickstart Quick Start Guide

 A quick start guide is located at: https://pegusapps.atlassian.net/wiki/pages/viewpage.action?pageId=9437252
 *
 \section classes Important interfaces and classes
 *
 - ect::Service
 The interface used to start and stop and configure the service gateway
 - ect::ServiceListener
 The interface used to listen for service activity such as the connection of a terminal
 - ect::Terminal
 Represents a terminal device. The interface is used to make transactions with the terminal
 - ect::ECTError
 Frequently used error object for error handling convenience
 *
 - ect::SaleTransaction
 The transaction class to make sale related transactions that don't require an authorization code
 - ect::SaleWithDeliveryConfirmationTransaction
 The transaction class to make sale with delivery confirmation related transactions that don't require an authorization code
 - ect::AuthorizedSaleTransaction
 The transaction class to make sale related transactions that require an authorization code
 - ect::CancelRetailTransaction
 The transaction class to cancel an ongoing retail transaction (before the validation by the customer)
 - ect::RequestCardInfoTransaction
 The transaction class to request card information. Returns for example the Clipped PAN, token, etc.
 - ect::CheckCardValidityTransaction
 The transaction class to check the validity of the card
 - ect::BalanceTerminalConsultationTransaction
 The transaction class to request the balance consultation
 - ect::PrintTicketTransaction
 The Transaction class to print a transaction ticket
 - ect::SaleWithCashbackTransaction
 The Transaction class to make a sale with cashback
 - ect::LastTransactionStatusTransaction
 The Transaction class to request Last Transaction Status related transaction

 *
 * @author Easy Integrator Development Team
 *
 * \section usage Usage
 *
 @brief
  The interface used to start and stop and configure the service gateway

 \par Creating the service
 To create an instance of this interface you need to call the factory method ect::MakeService
            You will also need to provide a listener interface to listen for terminal connections
 @code
 std::shared_ptr<Service> service = ect::MakeService(*this);
 @endcode

 \par Starting the service
 To start listening for a terminal connection, use the following code:
 @code
 service->startService(9000);
 @endcode

 \par Listening for connected terminals
 To listen for a terminal connection, you need to implement the ServiceListener interface
 @code
 void YourServiceListener::onTerminalConnect(std::shared_ptr<Terminal> terminal) {
    // store the terminal for later usage
 }

 void YourServiceListener::onTerminalDisconnect(std::shared_ptr<Terminal> terminal) {
    // do something
 }
 @endcode

 \par Make a simple _Sale_
 To make a sale transaction, use the following code:
 @code
 // C++14
 terminal->send(SaleTransaction(SaleAction::Sale, Amount("19.99"), "Merchant Reference", [](const auto& result) {
                                        // do something with the result
                                    }

 // C++11
 terminal->send(SaleTransaction(SaleAction::Sale, Amount("19.99"), "Merchant Reference", [](const SaleResult& result) {
                                        // do something with the result
                                    }
 @endcode

 \par Stop the service
 To stop the service, use the following code
 @code
 service->stopService();
 @endcode
 */

// transactions
#include "transactions/AuthorizedSaleTransaction.h"
#include "transactions/BackupModeStatusTransaction.h"
#include "transactions/BackupModeTransaction.h"
#include "transactions/BalanceTerminalConsultationTransaction.h"
#include "transactions/CancelPasswordValidation.h"
#include "transactions/CancelRetailTransaction.h"
#include "transactions/CheckCardValidityTransaction.h"
#include "transactions/LastTransactionStatusTransaction.h"
#include "transactions/PrintTicketTransaction.h"
#include "transactions/RequestCardInfoTransaction.h"
#include "transactions/ResetScreen.h"
#include "transactions/SaleTransaction.h"
#include "transactions/SaleWithCashbackTransaction.h"
#include "transactions/SaleWithDeliveryConfirmationTransaction.h"
#include "transactions/ServiceInformationTransaction.h"
#include "transactions/ShowMessage.h"
#include "transactions/SmartPaySaleTransaction.h"
#include "transactions/VerifyPin.h"
#include "transactions/VerifyPinFlow.h"

// command results
#include "transactions/results/AuthorizedSaleResult.h"
#include "transactions/results/BackupModeResult.h"
#include "transactions/results/BackupModeStatusResult.h"
#include "transactions/results/BalanceTerminalConsultationResult.h"
#include "transactions/results/CancelRetailResult.h"
#include "transactions/results/CheckCardValidityResult.h"
#include "transactions/results/LastTransactionResult.h"
#include "transactions/results/MMITransactionResult.h"
#include "transactions/results/PrintTicketResult.h"
#include "transactions/results/RequestCardInfoResult.h"
#include "transactions/results/SaleResult.h"
#include "transactions/results/SaleResultInfo.h"
#include "transactions/results/SaleWithCashbackResult.h"
#include "transactions/results/ServiceInformationResult.h"
#include "transactions/results/VerifyPinResult.h"

// ECT headers
#include "ECT/Amount.h"
#include "ECT/ECTData.h"
#include "ECT/ECTError.h"
#include "ECT/ECTFinancialCounterInfo.h"
#include "ECT/ECTIncidentCode.h"
#include "ECT/ECTRejectionCode.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"
#include "ECT/ECTTerminalEvent.hpp"
#include "ECT/MMIParameter.h"
//#include "ECT/NetworkSettings.h"
#include "ECT/Settings.h"
#include "ECT/Transaction.h"
#include "ECT/VersionInfo.h"
//#include "ECT/legacy/Service.h"
//#include "ECT/legacy/ServiceFactory.h"
//#include "ECT/legacy/ServiceListener.h"
//#include "ECT/legacy/Terminal.h"
#include "ECT/CTEPRecoveryFile.h"

// eik headers
#include "eik/CTEPService.h"
#include "eik/CTEPServiceFactory.h"
#include "eik/CTEPServiceListener.h"
#include "eik/CTEPTerminal.h"
