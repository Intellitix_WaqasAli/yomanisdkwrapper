#pragma once

#include "ECT/Transaction.h"
#include "ECT/ECTData.h"
#include <boost/optional.hpp>

namespace ect
{

class CancelRetailResult;


/*!
 To cancel a retail transaction you need to send the CancelRetailTransaction

 \par Example:
 @code
 // C++14
 terminal->send(CancelRetailTransaction([](const auto& result) {
                                    // do something with the result
                               }));

 // C++11
 terminal->send(CancelRetailTransaction([](const CancelRetailResult& result) {
                                    // do something with the result
                               }));
 }
@endcode
*/

/*!
 * \brief The CancelRetailTransaction is used to cancel a retail transaction
 */
class LIBRARY_API CancelRetailTransaction : public Transaction
{
public:
    /*!
     * \brief Constructor
     * \param resultCallback The result callback
     */
    CancelRetailTransaction(std::function<void (const CancelRetailResult&)> resultCallback);
    ~CancelRetailTransaction();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    CancelRetailTransaction(CancelRetailTransaction&&) = default;
    CancelRetailTransaction& operator=(CancelRetailTransaction&&) = default;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
