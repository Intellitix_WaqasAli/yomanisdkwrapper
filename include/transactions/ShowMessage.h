#include "ECT/ECTData.h"
#include "ECT/Transaction.h"
#include "ECT/FreeText.h"
#include <boost/optional.hpp>

namespace ect {
class MMITransactionResult;

/*!
   * \brief The ShowMessage class represents a Show message request

   \par Example:
   @code
   // C++14
   terminal->send(ShowMessage(screenID, languageCode, displayTimeout, [](const auto& result) {
   // do something with the result
   }));

   // C++11
   terminal->send(ShowMessage(screenID, languageCode, displayTimeout, [](const MMITransactionResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
class LIBRARY_API ShowMessage : public Transaction {
public:
    /*!
     * \brief ShowMessage Creates a transaction
     * \param screenIdentifier the identifier of the screen to show
     * \param languageCode the language code in which show the screens on terminal (en, fr, nl, de).
     * \param displayTimeout The display timeout (of the screen)
     * \param resultCallback The result callback
     */
    ShowMessage(int screenIdentifer, std::string languageCode, int displayTimeout, std::function<void(const MMITransactionResult&)> resultCallback);

    /*!
     * \brief ShowMessage Creates a transaction
     * \param screenIdentifier the identifier of the screen to show
     * \param languageCode the language code in which show the screens on terminal (en, fr, nl, de).
     * \param displayTimeout The display timeout (of the screen)
     * \param freeText a free text to show on the screen
     * \param resultCallback The result callback
     */
    ShowMessage(int screenIdentifer, std::string languageCode, int displayTimeout, FreeText freeText, std::function<void(const MMITransactionResult&)> resultCallback);

    ~ShowMessage();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    ShowMessage(ShowMessage&&) = default;
    ShowMessage& operator=(ShowMessage&&) = default;

private:
    ShowMessage() = default;
    struct Impl;
    std::shared_ptr<Impl> impl_;
};
}
