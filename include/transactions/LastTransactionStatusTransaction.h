#pragma once

#include "ECT/Transaction.h"
#include <boost/optional.hpp>
#include "ECT/ECTData.h"
#include "easyctepdefs.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"

namespace ect
{
  class LastTransactionResult;
  
  /*!
   * \brief The LastTransactionStatusTransaction class represents a Last Transaction Status related transaction
   
   \par Example: 
   @code
   // C++14
   terminal->send(LastTransactionStatusTransaction([](const auto& result) {
   // do something with the result
   }));
   
   // C++11
   terminal->send(LastTransactionStatusTransaction([](const LastTransactionResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
  class LIBRARY_API LastTransactionStatusTransaction : public Transaction
  {
  public:
    /*!
     * \brief LastTransactionStatusTransaction create a Last Transaction Status related transaction
     * \param resultCallback The callback to handle the transaction result
     */
    LastTransactionStatusTransaction(std::function<void (const LastTransactionResult&)> resultCallback);
    ~LastTransactionStatusTransaction();
    
    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    LastTransactionStatusTransaction(LastTransactionStatusTransaction&&) = default;
    LastTransactionStatusTransaction& operator=(LastTransactionStatusTransaction&&) = default;

  protected:
    LastTransactionStatusTransaction() = default;
    struct LIBRARY_API Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
