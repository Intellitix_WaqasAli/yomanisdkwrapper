#include "ECT/Transaction.h"
#include "ECT/ECTData.h"
#include <boost/optional.hpp>

namespace ect
{
  /*!
   * \brief The ResetScreen class represents a request for resetting the screen after a ShowMessage transaction
   
   \par Example:
   @code
   // C++14
   terminal->send(ResetScreen());
   
   // C++11
   terminal->send(ResetScreen());
   }
   @endcode
   */
  class LIBRARY_API ResetScreen : public Transaction
  {
  public:
    /*!
     * \brief ResetScreen Creates a transaction
     */
    ResetScreen();
    
    ~ResetScreen();
    
    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    ResetScreen(ResetScreen&&) = default;
    ResetScreen& operator=(ResetScreen&&) = default;

  private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
