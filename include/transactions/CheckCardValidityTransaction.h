#pragma once

#include <chrono>
#include <functional>
#include "ECT/Transaction.h"

namespace ect
{

class CheckCardValidityResult;


/*!
 * \brief The CheckCardValidityTransaction class represents a transaction to check the validity of a card
 *
 \par Example:
 @code
 // C++14
 terminal->send(CheckCardValidityTransaction([this](const auto& result) {
                                    handleResult(result);
                               }));

 // C++11
 terminal->send(CheckCardValidityTransaction([this](const CheckCardValidityResult& result) {
                                    handleResult(result);
                               }));
 }
 @endcode
 */
class LIBRARY_API CheckCardValidityTransaction : public Transaction
{
public:
    /*!
     * \brief CheckCardValidityTransaction Creates a CheckCardValidity Transaction
     * \param resultCallback The result callback
     */
    CheckCardValidityTransaction(std::function<void (const CheckCardValidityResult&)> resultCallback);
    ~CheckCardValidityTransaction();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    CheckCardValidityTransaction(CheckCardValidityTransaction&&) = default;
    CheckCardValidityTransaction& operator=(CheckCardValidityTransaction&&) = default;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
