#pragma once

#include "ECT/Transaction.h"
#include <chrono>
#include <functional>
#include <memory>

namespace ect {

class BalanceTerminalConsultationResult;

/*!
 * \brief The BalanceTerminalConsultationTransaction class represents a Balance Terminal Consultation transaction
 *
 \par Example:
 @code
 // C++14
 terminal->send(ect::BalanceTerminalConsultationTransaction(getAcquirerID(), [this](const auto& result) {
                                                    handleResult(result);
                                               }));

 // C++11
 terminal->send(ect::BalanceTerminalConsultationTransaction(getAcquirerID(), [this](const BalanceTerminalConsultationResult& result) {
                                                    handleResult(result);
                                               }));
 }
 @endcode
 */
class LIBRARY_API BalanceTerminalConsultationTransaction : public Transaction {
public:
    /*!
     * \brief BalanceTerminalConsultationTransaction Constructs a balance terminal consultation transaction
     * \param acquirerId The acquirer identifier
     * \param resultCallback The callback to handle the transaction result
     */
    BalanceTerminalConsultationTransaction(const std::string& acquirerId, std::function<void(const BalanceTerminalConsultationResult&)> resultCallback);
    ~BalanceTerminalConsultationTransaction();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    BalanceTerminalConsultationTransaction(BalanceTerminalConsultationTransaction&&) = default;
    BalanceTerminalConsultationTransaction& operator=(BalanceTerminalConsultationTransaction&&) = default;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};
}
