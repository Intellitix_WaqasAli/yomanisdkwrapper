#pragma once

#include <chrono>
#include <functional>
#include "ECT/Transaction.h"

namespace ect
{

class RequestCardInfoResult;

enum class CardInfoOption
{
    SaleAfterRecognition,
    NoSaleAfterRecognition
};

/*!
 * \brief The RequestCardInfoTransaction class represents a transaction request card info
 * 3 options are possible:
 *  - SaleAfterRecognition,
 *  - NoSaleAfterRecognition,
 *

 \par Example:
 @code
 // C++14
 terminal->send(RequestCardInfoTransaction(CardInfoOption::NoSaleAfterRecognition, [this](const auto& result) {
                                    handleResult(result);
                               }));

 // C++11
 terminal->send(RequestCardInfoTransaction(CardInfoOption::NoSaleAfterRecognition, [this](const RequestCardInfoResult& result) {
                                    handleResult(result);
                               }));
 }
 @endcode
 */
class LIBRARY_API RequestCardInfoTransaction : public Transaction
{
public:
    /*!
     * \brief RequestCardInfoTransaction Creates a transaction to request card info
     * \param option Make a sale after recognition or not
     * \param resultCallback The result callback
     */
    RequestCardInfoTransaction(CardInfoOption option, std::function<void (const RequestCardInfoResult&)> resultCallback);
    RequestCardInfoTransaction(CardInfoOption option, std::chrono::seconds recognitionTimeout, std::function<void (const RequestCardInfoResult&)> resultCallback);
    ~RequestCardInfoTransaction();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    RequestCardInfoTransaction(RequestCardInfoTransaction&&) = default;
    RequestCardInfoTransaction& operator=(RequestCardInfoTransaction&&) = default;

private:
    RequestCardInfoTransaction() = default;
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
