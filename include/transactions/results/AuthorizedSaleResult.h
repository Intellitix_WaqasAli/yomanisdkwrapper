#pragma once

#include <boost/optional.hpp>
#include "transactions/results/SaleResultInfo.h"
#include "ECT/ECTError.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"

namespace ect
{

enum class AuthorizedSaleAction;

/*!
 * \brief The AuthorizedSaleResult class contains the result of the transaction
 */
class LIBRARY_API AuthorizedSaleResult final : public SaleResultInfo
{
public:
    /*!
     * \brief AuthorizedSaleResult Creates the result
     * \param action The action type of the transaction
     * \param resultData serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    AuthorizedSaleResult(AuthorizedSaleAction action, const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief action Gets the action type of the original transaction
     * \return the action type
     */
    AuthorizedSaleAction action() const;
    /*!
     * \brief error Gets error information, if any.
     * \return the optional error
     */
    boost::optional<ECTError> error() const;

    /*!
     * \brief saleSystemActionIdentifier Get the sale system action identifier of the last transaction
     * \return the sale system action identifier
     */
    boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
