#pragma once

#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{
  
  /*!
   * \brief The BackupModeResult class contains the result information of the retail backup mode transaction
   */
  class LIBRARY_API BackupModeResult final
  {
  public:
    /*!
     * \brief BackupModeResult constructs the transaction result object
     * \param resultData the serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    BackupModeResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief timeStamp Get the timestamp of the transaction
     * \return the timestamp of the transaction in local time
     */
    tm timeStamp() const;
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;
    
  private:
    tm timeStamp_;
    std::string jsonResult_;
    std::string terminalIdentifier_;
    boost::optional<ECTError> error_;
  };
  
}
