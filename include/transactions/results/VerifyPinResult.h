#pragma once

#include "ECT/ECTError.h"
#include <boost/optional.hpp>
#include <memory>
#include <string>

namespace ect {

/*!
   * \brief The VerifyPinResult class contains the result information the transaction
   */
class LIBRARY_API VerifyPinResult final {
public:
    /*!
     * \brief VerifyPinResult constructs the result object
     * \param type the type of validation
     * \param resultData serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    VerifyPinResult(const std::string& type, const std::string& resultData, const std::string& terminalIdentifier);

    /**
     The number attempts left for the pin code.
     */
    int attemptsLeft() const;

    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
