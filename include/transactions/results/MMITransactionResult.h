#pragma once

#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{
  
  /*!
   * \brief The MMITransactionResult class contains the result information the transaction
   */
  class LIBRARY_API MMITransactionResult final
  {
  public:
    /*!
     * \brief MMITransactionResult constructs the result object
     * \param action the action (Open, Close, Request)
     * \param resultData serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    MMITransactionResult(const std::string& action, const std::string& resultData, const std::string& terminalIdentifier);

    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;
    
    
    /*!
     * \brief When true, it means the user pressed the Stop key instead of the Ok key
     * \return true if user pressed the Stop key
     */
    bool stopKeyPressed() const;

    /**
     * \brief When true, it means the user pressed the Ok key
     * \return true if user pressed the Ok key
     */
    bool okKeyPressed() const;
    
    
    /**
     * \brief When true, it means the operation has been aborted
     * \return true if the operation has been aborted
     */
    bool aborted() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

    
  private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
