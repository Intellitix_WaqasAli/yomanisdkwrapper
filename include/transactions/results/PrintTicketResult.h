#pragma once

#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{

/*!
 * \brief The PrintTicketResult class contains the result information of the related transaction
 */
class LIBRARY_API PrintTicketResult final
{
public:
    /*!
     * \brief PrintTicketResult constructs the result information
     * \param resultData the serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    PrintTicketResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
