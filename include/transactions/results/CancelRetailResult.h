#pragma once

#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{

/*!
 * \brief The CancelRetailResult class contains the result information of the cancel retail transaction
 */
class LIBRARY_API CancelRetailResult final
{
public:
    /*!
     * \brief CancelRetailResult constructs the transaction result object
     * \param resultData the serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    CancelRetailResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief timeStamp Get the timestamp of the transaction
     * \return the timestamp of the transaction in local time
     */
    tm timeStamp() const;
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    tm timeStamp_;
    std::string jsonResult_;
    boost::optional<ECTError> error_;
    std::string terminalIdentifier_;
};

}
