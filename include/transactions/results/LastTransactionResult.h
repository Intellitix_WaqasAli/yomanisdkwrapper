#pragma once

#include <boost/optional.hpp>
#include "ECT/Transaction.h"
#include "transactions/results/SaleResultInfo.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"
#include "easyctepdefs.h"

namespace ect
{

  class ECTError;
  class SaleResultInfo;

  /*!
   * \brief The LastTransactionResult class contains the result information of the related Last Transsaction Status transaction
   */
  class LIBRARY_API LastTransactionResult final : public SaleResultInfo
  {
  public:
    /*!
     * \brief LastTransactionResult constructs the result object
     * \param resultData The serialized result info
     * \param terminalIdentifier The identifier of the terminal
     */
    LastTransactionResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;
    /*!
     * \brief incidentCode Get the incident code of the last transaction
     * \return the incident code
     */
    std::string incidentCode() const;
    /*!
     * \brief saleSystemActionIdentifier Get the sale system action identifier of the last transaction
     * \return the sale system action identifier
     */
    boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

  private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };

}
