#pragma once

#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{
  
  /*!
   * \brief The ServiceInformationResult class contains the result information the transaction
   */
  class LIBRARY_API ServiceInformationResult final
  {
  public:
    /*!
     * \brief ServiceInformationResult constructs the result object
     * \param resultData serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    ServiceInformationResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief check if the cashback feature is enabled in the terminal
     * \return true if enabled, false if not
     */
    bool cashbackEnabled() const;
    boost::optional<ECTError> error() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

  private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
