#pragma once

#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{

class ECTError;

class LIBRARY_API CheckCardValidityResult final
{
public:
    /*!
     * \brief CheckCardValidityResult constructs the transaction result
     * \param resultData
     * \param terminalIdentifier The identifier of the terminal
     */
    CheckCardValidityResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief timeStamp Get the timestamp of the transaction
     * \return the timestamp of the transaction in local time
     */
    tm timeStamp() const;
    /*!
     * \brief clippedPAN Get the clippedPAN
     * \return the clippen PAN
     */
    std::string clippedPAN() const;
    /*!
     * \brief cardBrandName Get the brand name of the card, e.g.: VISA
     * \return the brand name of the card
     */
    std::string cardBrandName() const;
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
