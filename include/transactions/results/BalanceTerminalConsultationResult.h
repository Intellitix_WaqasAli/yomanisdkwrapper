#pragma once

#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "ECT/ECTError.h"
#include "ECT/ECTFinancialCounterInfo.h"

namespace ect
{

/*!
 * \brief The BalanceTerminalConsultationResult class contains the result information the transaction
 */
class LIBRARY_API BalanceTerminalConsultationResult final
{
public:
    /*!
     * \brief BalanceTerminalConsultationResult constructs the result object
     * \param resultData serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    BalanceTerminalConsultationResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief terminalId Gets the terminal identifiers
     * \return the terminal identifier
     */
    std::string terminalId() const;
    /*!
     * \brief acquirerId Get the acquirerId
     * \return the acquirer identifier
     */
    std::string acquirerId() const;
    /*!
     * \brief acquirerLabelName Get the label name of the acquirer
     * \return the acquirer label name
     */
    std::string acquirerLabelName() const;
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;
    /*!
     * \brief financialCounterInfos Get the financial counter information
     * \return List of financial counter information
     */
    std::vector<ECTFinancialCounterInfo> financialCounterInfo() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
