#pragma once

#include "ECT/ECTError.h"
#include "ECT/Transaction.h"
#include <boost/optional.hpp>
#include <ctime>
#include <vector>

namespace Json {
class Value;
}

namespace ect {

enum class SaleAction;

/*!
 * \brief The SaleResultInfo class contains detailed result information of the sale transaction
 */
class LIBRARY_API SaleResultInfo {
public:
    /*!
     * \brief SaleResultInfo constructs the sale result info
     * \param resultData serialized resultData
     */
    SaleResultInfo(const Json::Value& resultData);

    /*!
     * \brief timeStamp Get the timestamp of the transaction
     * \return the timestamp of the transaction in local time
     */
    boost::optional<std::tm> timeStamp() const;
    /*!
     * \brief authorizationCode Get the authorization code of the transaction
     * \return the authorization code
     */
    std::string authorizationCode() const;
    /*!
     * \brief authorizedAmount Get the authorized amount of the transaction
     * \return the authorized amount
     */
    double authorizedAmount() const;
    /*!
     * \brief acquirerDiscretionaryData Get the discrectionary data of the transaction
     * \return the discrectionary data
     */
    std::string acquirerDiscretionaryData() const;
    /*!
     * \brief acquirerId Get the acquirer identifier
     * \return the acquirer identifier
     */
    std::string acquirerId() const;
    /*!
     * \brief language Get the language
     * \return  the language, e.g.: EN for english
     */
    std::string language() const;
    /*!
     * \brief clippedPAN Get the clipped PAN
     * \return the clipped PAN
     */
    std::string clippedPAN() const;
    /*!
     * \brief cardBrandName Get the card's brand name
     * \return the card's brand name
     */
    std::string cardBrandName() const;
    /*!
     * \brief cardBrandIdentifier Get the card's brand identifier
     * \return the card's brand identifier
     */
    std::string cardBrandIdentifier() const;
    /*!
     * \brief clientTicket Get the content of the client ticket
     * \return the content of the client ticket
     */
    std::string clientTicket() const;
    /*!
     * \brief merchantTicket Get the content of the merchant's ticket
     * \return the content of the merchant's ticket
     */
    std::string merchantTicket() const;
    /*!
     * \brief merchantText Get the merchant text
     * \return the merchant text
     */
    std::string merchantText() const;
    /*!
     * \brief signatureRequired is a signature required
     * \return True if a signature is required
     */
    bool signatureRequired() const;
    /*!
     * \brief additionalAmount Get the optional additional amount
     * \return the optional additional amount
     */
    boost::optional<double> additionalAmount() const;
    /*!
     * \brief partialApproval Get the optional partial approval
     * \return the optional partial approval
     */
    boost::optional<bool> partialApproval() const;
    /*!
     * \brief proprietaryData Get the Proprietary Data, in bytes list format, null if no data
     * \return the Proprietary Data, in bytes list format, null if no data
     */
    boost::optional<std::vector<uint8_t>> proprietaryData() const;


    /*!
     * \brief isContactless
     * \return True if the card is contactless, false otherwise
     */
    bool isContactless() const;
    /*!
     * \brief isMagneticStripe
     * \return True if the card has a magnetic stripe, false otherwise
     */
    bool isMagneticStripe() const;
    /*!
     * \brief isIccEmv
     * \return True if it's ICC EMV card type, false otherwise
     */
    bool isIccEmv() const;
    /*!
     * \brief isIccNonEmv
     * \return True if it's a ICC non-EMV card type, false otherwise
     */
    bool isIccNonEmv() const;
    /*!
     * \brief isBarCode
     * \return True if it's a bar code, false otherwise
     */
    bool isBarCode() const;
    /*!
     * \brief isManualEntry
     * \return True if it's a manual card entry, false otherwise
     */
    bool isManualEntry() const;
    /*!
     * \brief isSynchronousChip
     * \return True if it's a syncronous card, false otherwise
     */
    bool isSynchronousChip() const;
    /*!
     * \brief isAsynchronousChip
     * \return True if it's an asynchronous card, false otherwise
     */
    bool isAsynchronousChip() const;

    /*!
     * \brief processedInBackupMode
     * \return True if the transaction was processed in backup mode, false otherwise
     */
    bool processedInBackupMode() const;



    std::string ticketDiscreteElements() const;
    std::string ticketLayoutIdentifier() const;

    void updateClientTicket(const std::string& ticket) const;

protected:
    std::vector<uint8_t> HexToBytes(const std::string& hex) const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};
}
