#pragma once

#include <string>
#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{

class ECTError;

/*!
 * \brief The RequestCardInfoResult class contains the result information of the related transaction
 */
class LIBRARY_API RequestCardInfoResult final
{
public:
    /*!
     * \brief RequestCardInfoResult constructs the result object
     * \param resultData the serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    RequestCardInfoResult(const std::string& resultData, const std::string& terminalIdentifier);
    ~RequestCardInfoResult();
    /*!
     * \brief timeStamp Get the timestamp of the transaction
     * \return the timestamp of the transaction in local time
     */
    tm timeStamp() const;
    /*!
     * \brief clippedPAN Get the clippedPAN
     * \return the clippen PAN
     */
    std::string clippedPAN() const;
    /*!
     * \brief token Get the token
     * \return the token
     */
    std::string token() const;
    /*!
     * \brief isContactless
     * \return True if the card is contactless, false otherwise
     */
    bool isContactless() const;
    /*!
     * \brief isMagneticStripe
     * \return True if the card has a magnetic stripe, false otherwise
     */
    bool isMagneticStripe() const;
    /*!
     * \brief isIccEmv
     * \return True if it's ICC EMV card type, false otherwise
     */
    bool isIccEmv() const;
    /*!
     * \brief isIccNonEmv
     * \return True if it's a ICC non-EMV card type, false otherwise
     */
    bool isIccNonEmv() const;
    /*!
     * \brief isBarCode
     * \return True if it's a bar code, false otherwise
     */
    bool isBarCode() const;
    /*!
     * \brief isManualEntry
     * \return True if it's a manual card entry, false otherwise
     */
    bool isManualEntry() const;
    /*!
     * \brief isSynchronousChip
     * \return True if it's a syncronous card, false otherwise
     */
    bool isSynchronousChip() const;
    /*!
     * \brief isAsynchronousChip
     * \return True if it's an asynchronous card, false otherwise
     */
    bool isAsynchronousChip() const;
    /*!
     * \brief cardBrandName Get the card's brand name
     * \return the card's brand name
     */
    std::string cardBrandName() const;
    /*!
     * \brief cardBrandIdentifier Get the card's brand identifier
     * \return the card's brand identifier
     */
    std::string cardBrandIdentifier() const;
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
