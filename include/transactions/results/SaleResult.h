#pragma once

#include <boost/optional.hpp>
#include "ECT/Transaction.h"
#include "transactions/results/SaleResultInfo.h"
#include "easyctepdefs.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"

namespace ect
{

class ECTError;
class SaleResultInfo;

/*!
 * \brief The SaleResult class contains the result information of the related sale transaction
 */
class LIBRARY_API SaleResult final : public SaleResultInfo
{
public:
    /*!
     * \brief SaleResult constructs the result object
     * \param action The transaction sale action type
     * \param resultData The serialized result onfo
     * \param terminalIdentifier The identifier of the terminal
     */
    SaleResult(SaleAction action, const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief action Gets the action type of the original transaction
     * \return the action type
     */
    SaleAction action() const;
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;
    /*!
     * \brief saleSystemActionIdentifier Get the sale system action identifier of the last transaction
     * \return the sale system action identifier
     */
    boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
