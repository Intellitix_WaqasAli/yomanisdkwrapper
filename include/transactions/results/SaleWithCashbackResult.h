#pragma once

#include <boost/optional.hpp>
#include "ECT/Transaction.h"
#include "transactions/results/SaleResultInfo.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"
#include "easyctepdefs.h"

namespace ect
{
  
  class ECTError;
  class SaleResultInfo;
  
  /*!
   * \brief The SaleWithCashbackResult class contains the result information of the related sale with cashback transaction
   */
  class LIBRARY_API SaleWithCashbackResult final : public SaleResultInfo
  {
  public:
    /*!
     * \brief SaleWithCashbackResult constructs the result object
     * \param resultData The serialized result onfo
     * \param terminalIdentifier The identifier of the terminal
     */
    SaleWithCashbackResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;
    /*!
     * \brief saleSystemActionIdentifier Get the sale system action identifier of the last transaction
     * \return the sale system action identifier
     */
    boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

  private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
