#pragma once

#include <boost/optional.hpp>
#include "ECT/ECTError.h"

namespace ect
{
  
  /*!
   * \brief The BackupModeStatusResult class contains the result information of the retail backup mode status transaction
   */
  class LIBRARY_API BackupModeStatusResult final
  {
  public:
    /*!
     * \brief BackupModeStatusResult constructs the transaction result object
     * \param resultData the serialized result data
     * \param terminalIdentifier The identifier of the terminal
     */
    BackupModeStatusResult(const std::string& resultData, const std::string& terminalIdentifier);
    /*!
     * \brief timeStamp Get the timestamp of the transaction
     * \return the timestamp of the transaction in local time
     */
    tm timeStamp() const;
    /*!
     * \brief error Get the error, if any
     * \return the optional error
     */
    boost::optional<ECTError> error() const;
    /*!
     * \brief returns true if the backup mode is enabled on the terminal, false otherwise
     * \return true if the backup mode is enabled on the terminal, false otherwise
     */
    bool enabled() const;

    /*!
     * \brief Return the identifier of the terminal used for the transaction
     * \return the identifier of the terminal used for the transaction
     */
    std::string terminalIdentifier() const;

  private:
    tm timeStamp_;
    std::string jsonResult_;
    boost::optional<ECTError> error_;
    bool enabled_;
    std::string terminalIdentifier_;
  };
  
}
