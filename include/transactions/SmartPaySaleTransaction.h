#pragma once

#include "ECT/Transaction.h"
#include <boost/optional.hpp>
#include "ECT/Amount.h"
#include "ECT/ECTData.h"
#include "easyctepdefs.h"
#include "ECT/ECTEnums.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"

namespace ect
{
  class SaleResult;
  class Amount;
  
  /*!
   * \brief The SmartPaySaleTransaction class represents a sale with SmartPay transaction
   
   \par Example: Make a simple sale
   @code
   // C++14
   terminal->send(SmartPaySaleTransaction(Amount("19,99"), "the merchant reference", [](const auto& result) {
   // do something with the result
   }));
   
   // C++11
   terminal->send(SmartPaySaleTransaction(Amount("19,99"), "the merchant reference", [](const SaleResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
  class LIBRARY_API SmartPaySaleTransaction : public Transaction
  {
  public:
    /*!
     * \brief SmartPaySaleTransaction create a sale related transaction
     * \param amount The amount
     * \param resultCallback The callback to handle the transaction result
     */
    SmartPaySaleTransaction(const Amount& amount, std::function<void (const SaleResult&)> resultCallback);
    /*!
     * \brief SmartPaySaleTransaction create a sale related transaction
     * \param amount The amount
     * \param merchant The merchant reference
     * \param resultCallback The callback to handle the transaction result
     */
    SmartPaySaleTransaction(const Amount& amount, const std::string& merchant, std::function<void (const SaleResult&)> resultCallback);
    ~SmartPaySaleTransaction();
    
    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    SmartPaySaleTransaction(SmartPaySaleTransaction&&) = default;
    SmartPaySaleTransaction& operator=(SmartPaySaleTransaction&&) = default;

  protected:
    SmartPaySaleTransaction() = default;
    struct LIBRARY_API Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
