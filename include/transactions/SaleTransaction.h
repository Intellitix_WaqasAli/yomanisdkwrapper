#pragma once

#include "ECT/Amount.h"
#include "ECT/ECTData.h"
#include "ECT/ECTEnums.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"
#include "ECT/Transaction.h"
#include "ECT/cardbrandidentifier.hpp"
#include "easyctepdefs.h"
#include <boost/optional.hpp>

namespace ect {

/*!
 * \brief The SaleAction enum represents the possible sale transaction types that don't need any authorization
 */
enum class LIBRARY_API SaleAction {
    Sale,
    SaleWithDeliveryConfirmation,
    Reservation,
    Refund,
    CashAdvance,
    Unknown
};

class SaleResult;
class Amount;

/*!
 * \brief The SaleTransaction class represents a sale related transaction
 * Several types of related transaction types are possible:
 *  - Sale,
 *  - Reservation,
 *  - Refund,
 *  - CashAdvance
 *

 \par Example: Make a simple sale
 @code
 // C++14
 terminal->send(SaleTransaction(SaleAction::Sale, Amount("19,99"), "the merchant reference", [](const auto& result) {
                                    // do something with the result
                               }));

 // C++11
 terminal->send(SaleTransaction(SaleAction::Sale, Amount("19,99"), "the merchant reference", [](const SaleResult& result) {
                                    // do something with the result
                               }));
 }
 @endcode
 */
class LIBRARY_API SaleTransaction : public Transaction {
public:
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param merchant The merchant reference
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, const std::string& merchant, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param merchant The merchant reference
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, const std::string& merchant, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param merchant The merchant reference
     * \param discretionaryData
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, const std::string& merchant, const ect::ECTData& discretionaryData, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param merchant The merchant reference
     * \param discretionaryData
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, const std::string& merchant, const ect::ECTData& discretionaryData, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param discretionaryData The discrentionary data
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, const ect::ECTData& discretionaryData, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param discretionaryData The discrentionary data
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, const ect::ECTData& discretionaryData, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discrentionary data
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, std::function<void(const SaleResult&)> resultCallback);
    /*!
     * \brief SaleTransaction create a sale related transaction
     * \param action The action type
     * \param amount The amount
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discrentionary data
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleTransaction(SaleAction action, const Amount& amount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback);
    ~SaleTransaction();

    /*!
     * \brief Add a filter for the card brand identifier to the transaction.
     * \param filter The type of card brand to allow.
     * \return The transaction updated with the filter.
     */
    SaleTransaction&& addCardIdentifierFilter(CardBrandIdentifier filter);

    /*!
     * \brief Add a filter for the card brand identifiers to the transaction.
     * \param filters The types of card brand to allow.
     * \return The transaction updated with the filter.
     */
    SaleTransaction&& addCardIdentifierFilters(std::vector<CardBrandIdentifier> filters);

    /*!
     * \brief Add some Proprietary Data to the transaction.
     * \param data the Proprietary Data to add, in bytes list format
     * \return the updated transaction
     */
    SaleTransaction&& addProprietaryData(ECTData& data);

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    SaleTransaction(SaleTransaction&&) = default;
    SaleTransaction& operator=(SaleTransaction&&) = default;

protected:
    SaleTransaction() = default;

    struct LIBRARY_API Impl;
    std::shared_ptr<Impl> impl;
};
}
