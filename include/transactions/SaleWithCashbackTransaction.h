#pragma once

#include "ECT/Amount.h"
#include "ECT/ECTData.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"
#include "ECT/Transaction.h"
#include "ECT/cardbrandidentifier.hpp"
#include "easyctepdefs.h"
#include <boost/optional.hpp>

namespace ect {
class SaleWithCashbackResult;
class Amount;

/*!
   * \brief The SaleWithCashbackTransaction class represents a sale transaction, with a cashback

   \par Example: Make a simple sale with cashback
   @code
   // C++14
   terminal->send(SaleWithCashbackTransaction(Amount("19,99"), Amount("5,00"), "the merchant reference", [](const auto& result) {
   // do something with the result
   }));

   // C++11
   terminal->send(SaleWithCashbackTransaction(Amount("19,99"), Amount("5,00"), "the merchant reference", [](const SaleResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
class LIBRARY_API SaleWithCashbackTransaction : public Transaction {
public:
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param merchant The merchant reference
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, const std::string& merchant, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param merchant The merchant reference
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, const std::string& merchant, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param merchant The merchant reference
     * \param discretionaryData
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, const std::string& merchant, const ect::ECTData& discretionaryData, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param merchant The merchant reference
     * \param discretionaryData
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, const std::string& merchant, const ect::ECTData& discretionaryData, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param discretionaryData The discrentionary data
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, const ect::ECTData& discretionaryData, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param discretionaryData The discrentionary data
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, const ect::ECTData& discretionaryData, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale related transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discrentionary data
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    /*!
     * \brief SaleWithCashbackTransaction create a sale related transaction
     * \param amount The amount
     * \param cashbackAmount The amount of the cashback
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discrentionary data
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     */
    SaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const SaleWithCashbackResult&)> resultCallback);
    ~SaleWithCashbackTransaction();

    /*!
     * \brief Add a filter for the card brand identifier to the transaction.
     * \param filter The type of card brand to allow.
     * \return The transaction updated with the filter.
     */
    SaleWithCashbackTransaction&& addCardIdentifierFilter(CardBrandIdentifier filter);

    /*!
     * \brief Add a filter for the card brand identifiers to the transaction.
     * \param filters The types of card brand to allow.
     * \return The transaction updated with the filter.
     */
    SaleWithCashbackTransaction&& addCardIdentifierFilters(std::vector<CardBrandIdentifier> filters);

    /*!
     * \brief Add some Proprietary Data to the transaction.
     * \param data the Proprietary Data to add, in bytes list format
     * \return the updated transaction
     */
    SaleWithCashbackTransaction&& addProprietaryData(ECTData& data);

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    SaleWithCashbackTransaction(SaleWithCashbackTransaction&&) = default;
    SaleWithCashbackTransaction& operator=(SaleWithCashbackTransaction&&) = default;

private:
    SaleWithCashbackTransaction() = default;
    struct LIBRARY_API Impl;
    std::shared_ptr<Impl> impl_;
};

LIBRARY_API std::unique_ptr<SaleWithCashbackTransaction> MakeSaleWithCashbackTransaction(const Amount& amount, const Amount& cashbackAmount, std::function<void(const SaleWithCashbackResult&)> resultCallback);
}
