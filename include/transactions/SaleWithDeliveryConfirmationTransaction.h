#pragma once

#include "ECT/Amount.h"
#include "ECT/ECTData.h"
#include "ECT/ECTEnums.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"
#include "ECT/Transaction.h"
#include "SaleTransaction.h"
#include "easyctepdefs.h"
#include <boost/optional.hpp>

namespace ect {

class SaleResult;
class Amount;

/*!
   * \brief The SaleWithDeliveryConfirmationTransaction class represents a sale with delivery confirmation related transaction
   *

   \par Example: Make a simple sale with delivery confirmation
   @code
   // C++14
   terminal->send(SaleWithDeliveryConfirmationTransaction(Amount("19,99"), "the merchant reference", [](const auto& result) {
   // do something with the result
   }, [](const auto& result) {
   // do something with the result of the delivery request
   // Return ProductDeliveryBlockReturnValueAccepted to accept the delivery, ProductDeliveryBlockReturnValueRejected to reject the delivery
   }));

   // C++11
   terminal->send(SaleWithDeliveryConfirmationTransaction(Amount("19,99"), "the merchant reference", [](const SaleResult& result) {
   // do something with the result
   }, [](const SaleResult& result) {
   // do something with the result of the delivery request
   // Return ProductDeliveryBlockReturnValueAccepted to accept the delivery, ProductDeliveryBlockReturnValueRejected to reject the delivery
   }));
   }
   @endcode
   */
class LIBRARY_API SaleWithDeliveryConfirmationTransaction : public Transaction {
public:
    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);

    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);

    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param merchant The merchant reference
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, const std::string& merchant, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);

    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param merchant The merchant reference
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, const std::string& merchant, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);

    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param merchant The merchant reference
     * \param discretionaryData
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, const std::string& merchant, const ect::ECTData& discretionaryData, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);
    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param merchant The merchant reference
     * \param discretionaryData
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, const std::string& merchant, const ect::ECTData& discretionaryData, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);

    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param discretionaryData The discrentionary data
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, const ect::ECTData& discretionaryData, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);
    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param discretionaryData The discrentionary data
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, const ect::ECTData& discretionaryData, const ECTSaleSystemActionIdentifier& saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);

    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discrentionary data
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);
    /*!
     * \brief SaleWithDeliveryConfirmationTransaction create a sale with delivery confirmation related transaction
     * \param amount The amount
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discrentionary data
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The callback to handle the transaction result
     * \param productDeliveryResultCallback The callback called by the service to request a delivery confirmation
     */
    SaleWithDeliveryConfirmationTransaction(const Amount& amount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const SaleResult&)> resultCallback, std::function<ProductDeliveryBlockReturnValue(const SaleResult&)> productDeliveryResultCallback);

    ~SaleWithDeliveryConfirmationTransaction();

    /*!
     * \brief Add a filter for the card brand identifier to the transaction.
     * \param filter The type of card brand to allow.
     * \return The transaction updated with the filter.
     */
    SaleWithDeliveryConfirmationTransaction&& addCardIdentifierFilter(CardBrandIdentifier filter);

    /*!
     * \brief Add a filter for the card brand identifiers to the transaction.
     * \param filters The types of card brand to allow.
     * \return The transaction updated with the filter.
     */
    SaleWithDeliveryConfirmationTransaction&& addCardIdentifierFilters(std::vector<CardBrandIdentifier> filters);

    /*!
     * \brief Add some Proprietary Data to the transaction.
     * \param data the Proprietary Data to add, in bytes list format
     * \return the updated transaction
     */
    SaleWithDeliveryConfirmationTransaction&& addProprietaryData(ECTData& data);


    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    SaleWithDeliveryConfirmationTransaction(SaleWithDeliveryConfirmationTransaction&&) = default;
    SaleWithDeliveryConfirmationTransaction& operator=(SaleWithDeliveryConfirmationTransaction&&) = default;

private:
    SaleWithDeliveryConfirmationTransaction() = default;
    struct LIBRARY_API Impl;
    std::shared_ptr<Impl> impl_;
};
}
