#include "ECT/ECTData.h"
#include "ECT/EncryptedData.h"
#include "ECT/FreeText.h"
#include "ECT/Transaction.h"
#include <boost/optional.hpp>

namespace ect {
class VerifyPinResult;

/*!
   * \brief The VerifyPin class represents a Check PIN transaction

   \par Example:
   @code
   // C++14
   terminal->send(VerifyPin(encryptedPin, screenID, languageCode, displayTimeout, [](const auto& result) {
   // do something with the result
   }));

   // C++11
   terminal->send(VerifyPin(encryptedPin, screenID, languageCode, displayTimeout, [](const VerifyPinResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
class LIBRARY_API VerifyPin : public Transaction {
public:
    /*!
     * \brief VerifyPin Creates a transaction
     * \param encryptedPin the PIN code to check.
     * \param screenIdentifier the identifier of the screen to show
     * \param languageCode the language code in which show the screens on terminal (en, fr, nl, de).
     * \param displayTimeout The display timeout (of the screen)
     * \param resultCallback The result callback
     */
    VerifyPin(EncryptedData encryptedPin, int screenIdentifer, std::string languageCode, int displayTimeout, std::function<void(const VerifyPinResult&)> resultCallback);

    /*!
     * \brief VerifyPin Creates a transaction
     * \param encryptedPin the PIN code to check.
     * \param screenIdentifier the identifier of the screen to show
     * \param languageCode the language code in which show the screens on terminal (en, fr, nl, de).
     * \param displayTimeout The display timeout (of the screen)
     * \param freeText a free text to show on the screen
     * \param resultCallback The result callback
     */
    VerifyPin(EncryptedData encryptedPin, int screenIdentifer, std::string languageCode, int displayTimeout, FreeText freeText, std::function<void(const VerifyPinResult&)> resultCallback);

    ~VerifyPin();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> /*responseCallback*/) override;
    std::unique_ptr<Transaction> move() override;


    VerifyPin(VerifyPin&&) = default;
    VerifyPin& operator=(VerifyPin&&) = default;

private:
    VerifyPin() = default;
    struct Impl;
    std::shared_ptr<Impl> impl_;
};
}
