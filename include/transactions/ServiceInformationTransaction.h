#pragma once

#include <chrono>
#include <functional>
#include "ECT/Transaction.h"
#include <memory>

namespace ect
{
  
  class ServiceInformationResult;
  
  /*!
   * \brief The ServiceInformationTransaction class represents a Service Information transaction
   *
   \par Example:
   @code
   // C++14
   terminal->send(ect::ServiceInformationTransaction([this](const auto& result) {
   handleResult(result);
   }));
   
   // C++11
   terminal->send(ect::ServiceInformationTransaction([this](const ServiceInformationResult& result) {
   handleResult(result);
   }));
   }
   @endcode
   */
  class LIBRARY_API ServiceInformationTransaction : public Transaction
  {
  public:
    /*!
     * \brief ServiceInformationTransaction Constructs a Service Information transaction
     * \param resultCallback The callback to handle the transaction result
     */
    ServiceInformationTransaction(std::function<void (const ServiceInformationResult&)> resultCallback);
    ~ServiceInformationTransaction();
    
    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    ServiceInformationTransaction(ServiceInformationTransaction&&) = default;
    ServiceInformationTransaction& operator=(ServiceInformationTransaction&&) = default;

  private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
