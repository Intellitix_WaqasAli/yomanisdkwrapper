#pragma once

#include "ECT/ECTData.h"
#include "ECT/Transaction.h"
#include "easyctepdefs.h"
#include <boost/optional.hpp>

namespace ect {
class BackupModeStatusResult;

/*!
   * \brief The BackupModeStatusTransaction class represents a Backup Mode Status related transaction

   \par Example:
   @code
   // C++14
   bool enable = true;
   terminal->send(BackupModeStatusTransaction("1234", [](const auto& result) {
   // do something with the result
   }));

   // C++11
   bool enable = true;
   terminal->send(BackupModeStatusTransaction("1234", [](const BackupModeStatusResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
class LIBRARY_API BackupModeStatusTransaction : public Transaction {
public:
    /*!
     * \brief BackupModeStatusTransaction create a Backup Mode Status related transaction
     * \param enable true to enable the backup mode, false to disable it
     * \param resultCallback The callback to handle the transaction result
     */
    BackupModeStatusTransaction(std::function<void(const BackupModeStatusResult&)> resultCallback);
    ~BackupModeStatusTransaction();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    BackupModeStatusTransaction(BackupModeStatusTransaction&&) = default;
    BackupModeStatusTransaction& operator=(BackupModeStatusTransaction&&) = default;

protected:
    BackupModeStatusTransaction() = default;
    struct LIBRARY_API Impl;
    std::shared_ptr<Impl> impl_;
};
}
