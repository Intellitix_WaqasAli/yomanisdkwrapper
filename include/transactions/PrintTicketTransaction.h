#pragma once

#include <chrono>
#include <functional>
#include "ECT/Transaction.h"

namespace ect
{

class PrintTicketResult;

/*!
 * \brief The PrintTicketTransaction class represents a transaction to print a ticket
 *
 \par Example:
 @code
 // C++14
 terminal->send(PrintTicketTransaction([this](const auto& result) {
                                    handleResult(result);
                               }));

 // C++11
 terminal->send(PrintTicketTransaction([this](const PrintTicketResult& result) {
                                    handleResult(result);
                               }));
 }
 @endcode
 */
class LIBRARY_API PrintTicketTransaction : public Transaction
{
public:
    /*!
     * \brief PrintTicketTransaction Creates a transaction to print a ticket
     * \param text The text to print
     * \param resultCallback The result callback
     */
    PrintTicketTransaction(const std::string& text, std::function<void (const PrintTicketResult&)> resultCallback);
    ~PrintTicketTransaction();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    PrintTicketTransaction(PrintTicketTransaction&&) = default;
    PrintTicketTransaction& operator=(PrintTicketTransaction&&) = default;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};

}
