#include "ECT/ECTData.h"
#include "ECT/Transaction.h"
#include <boost/optional.hpp>

namespace ect {
class CancelPasswordValidation;

/*!
   * \brief The CancelPasswordValidation class represents a Cancel Check PIN transaction

   \par Example:
   @code
   // C++14
   terminal->send(CancelPasswordValidation();

   // C++11
   terminal->send(CancelPasswordValidation();
   }
   @endcode
   */
class LIBRARY_API CancelPasswordValidation : public Transaction {
public:
    /*!
     * \brief CancelPasswordValidation Creates a request
     */
    CancelPasswordValidation();

    ~CancelPasswordValidation();

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    CancelPasswordValidation(CancelPasswordValidation&&) = default;
    CancelPasswordValidation& operator=(CancelPasswordValidation&&) = default;

private:
    struct Impl;
    std::shared_ptr<Impl> impl_;
};
}
