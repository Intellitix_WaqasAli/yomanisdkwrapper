#pragma once

#include "ECT/Transaction.h"
#include <boost/optional.hpp>
#include "ECT/ECTData.h"
#include "easyctepdefs.h"

namespace ect
{
  class BackupModeResult;
  
  /*!
   * \brief The BackupModeTransaction class represents a Backup Mode related transaction
   
   \par Example:
   @code
   // C++14
   bool enable = true;
   terminal->send(BackupModeTransaction(enable, "1234", [](const auto& result) {
   // do something with the result
   }));
   
   // C++11
   bool enable = true;
   terminal->send(BackupModeTransaction(enable, "1234", [](const BackupModeResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
  class LIBRARY_API BackupModeTransaction : public Transaction
  {
  public:
    /*!
     * \brief BackupModeTransaction create a Backup Mode related transaction
     * \param enable true to enable the backup mode, false to disable it
     * \param resultCallback The callback to handle the transaction result
     */
    BackupModeTransaction(bool enable, std::function<void (const BackupModeResult&)> resultCallback);
    ~BackupModeTransaction();
    
    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    BackupModeTransaction(BackupModeTransaction&&) = default;
    BackupModeTransaction& operator=(BackupModeTransaction&&) = default;

  protected:
    BackupModeTransaction() = default;
    struct LIBRARY_API Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
