#pragma once

#include "ECT/ECTData.h"
#include "ECT/ECTSaleSystemActionIdentifier.h"
#include "ECT/Transaction.h"
#include "ECT/cardbrandidentifier.hpp"
#include <boost/optional.hpp>

namespace ect {

class Amount;
class AuthorizedSaleResult;

/*!
 * \brief The AuthorizedSaleAction enum represents all possible sale actions that need an authorization code
 */
enum class LIBRARY_API AuthorizedSaleAction {
    SaleAfterVoiceReferral,
    SaleAfterReservation,
    CashAdvanceAfterVoiceReferral,
    CancelSale,
    CancelReservation,
    CancelSaleAfterReservation,
    CancelCashAdvance,
    Unknown
};

/*!
 * \brief The AuthorizedSaleTransaction class represents a sale related transaction that needs an authorization code
 * Several types of related transaction types are possible:
 *  - SaleAfterVoiceReferral,
 *  - SaleAfterReservation,
 *  - CashAdvanceAfterVoiceReferral,
 *  - CancelSale,
 *  - CancelReservation,
 *  - CancelSaleAfterReservation,
 *  - CancelCashAdvance

 \par Example: Make a simple cancel sale with authorization code
 @code
 // C++14
 terminal->send(AuthorizedSaleTransaction(AuthorizedSaleAction::CancelSale, Amount("19,99"), "the merchant reference", "<<<your authorization code>>>", [](const auto& result) {
                                    // do something with the result
                               }));

 // C++11
 terminal->send(AuthorizedSaleTransaction(AuthorizedSaleAction::CancelSale, Amount("19,99"), "the merchant reference",  "<<<your authorization code>>>", [](const AuthorizedSaleResult& result) {
                                    // do something with the result
                               }));
 }
 @endcode
 */
class LIBRARY_API AuthorizedSaleTransaction : public Transaction {
public:
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param authorizationCode The authorization code
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const std::string& authorizationCode, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param merchant The merchant reference
     * \param authorizationCode The authorization code
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const std::string& merchant, const std::string& authorizationCode, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param merchant The merchant reference
     * \param discretionaryData The discretionary data
     * \param authorizationCode The authorization code
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const std::string& merchant, const ect::ECTData& discretionaryData, const std::string& authorizationCode, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param discretionaryData The discretionary data
     * \param authorizationCode The authorization code
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const ect::ECTData& discretionaryData, const std::string& authorizationCode, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discretionary data
     * \param authorizationCode The authorization code
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, const std::string& authorizationCode, std::function<void(const AuthorizedSaleResult&)> resultCallback);

    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param authorizationCode The authorization code
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const std::string& authorizationCode, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param merchant The merchant reference
     * \param authorizationCode The authorization code
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const std::string& merchant, const std::string& authorizationCode, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param merchant The merchant reference
     * \param discretionaryData The discretionary data
     * \param authorizationCode The authorization code
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const std::string& merchant, const ect::ECTData& discretionaryData, const std::string& authorizationCode, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param discretionaryData The discretionary data
     * \param authorizationCode The authorization code
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, const ect::ECTData& discretionaryData, const std::string& authorizationCode, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const AuthorizedSaleResult&)> resultCallback);
    /*!
     * \brief AuthorizedSaleTransaction Creates a sale related transaction that needs an authorization code
     * \param action The sale action
     * \param amount The amount
     * \param merchant The optional merchant reference
     * \param discretionaryData The optional discretionary data
     * \param authorizationCode The authorization code
     * \param saleSystemActionIdentifier the Sale system action identifier used to identify uniquely  this transaction
     * \param resultCallback The result callback
     */
    AuthorizedSaleTransaction(AuthorizedSaleAction action, const Amount& amount, boost::optional<std::string> merchant, boost::optional<ect::ECTData> discretionaryData, const std::string& authorizationCode, boost::optional<ECTSaleSystemActionIdentifier> saleSystemActionIdentifier, std::function<void(const AuthorizedSaleResult&)> resultCallback);

    ~AuthorizedSaleTransaction();

    /*!
     * \brief Add a filter for the card brand identifier to the transaction.
     * \param filter The type of card brand to allow.
     * \return The transaction updated with the filter.
     */
    AuthorizedSaleTransaction&& addCardIdentifierFilter(CardBrandIdentifier filter);

    /*!
     * \brief Add a filter for the card brand identifiers to the transaction.
     * \param filters The types of card brand to allow.
     * \return The transaction updated with the filter.
     */
    AuthorizedSaleTransaction&& addCardIdentifierFilters(std::vector<CardBrandIdentifier> filters);

    /*!
     * \brief Add some Proprietary Data to the transaction.
     * \param data the Proprietary Data to add, in bytes list format
     * \return the updated transaction
     */
    AuthorizedSaleTransaction&& addProprietaryData(ECTData& data);

    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)> responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    AuthorizedSaleTransaction(AuthorizedSaleTransaction&&) = default;
    AuthorizedSaleTransaction& operator=(AuthorizedSaleTransaction&&) = default;

private:
    AuthorizedSaleTransaction() = default;
    struct Impl;
    std::shared_ptr<Impl> impl;
};
}
