#include "ECT/Transaction.h"
#include "ECT/ECTData.h"
#include "ECT/EncryptedData.h"
#include "ECT/FreeText.h"
#include <boost/optional.hpp>

namespace ect
{
  class VerifyPinResult;
  
  /*!
   * \brief The VerifyPinFlow class represents a Check PIN transaction with retries
   
   \par Example:
   @code
   // C++14
   terminal->send(VerifyPinFlow(encryptedPin, languageCode, displayTimeout, maximumTries, [](const auto& result) {
   // do something with the result
   }));

   // C++11
   terminal->send(VerifyPinFlow(encryptedPin, languageCode, displayTimeout, maximumTries, [](const VerifyPinResult& result) {
   // do something with the result
   }));
   }
   @endcode
   */
  class LIBRARY_API VerifyPinFlow : public Transaction
  {
  public:
    /*!
     * \brief VerifyPinFlow Creates a request
     * \param encryptedPin the PIN code to check.
     * \param languageCode the language code in which show the screens on terminal (en, fr, nl, de).
     * \param displayTimeout The display timeout (of the screen)
     * \param maximumTries The number of tries the user has to enter its pin code
     * \param keepSession if set to true the MMI session will be kept open.
     * \param resultCallback The result callback
     */
    VerifyPinFlow(EncryptedData encryptedPin, std::string languageCode, int displayTimeout, int maximumTries, bool keepSession, std::function<void (const VerifyPinResult&)> resultCallback);

    /*!
     * \brief VerifyPinFlow Creates a request
     * \param encryptedPin the PIN code to check.
     * \param languageCode the language code in which show the screens on terminal (en, fr, nl, de).
     * \param displayTimeout The display timeout (of the screen)
     * \param maximumTries The number of tries the user has to enter its pin code
     * \param freeText a free text to show on the screen
     * \param keepSession if set to true the MMI session will be kept open.
     * \param resultCallback The result callback
     */
    VerifyPinFlow(EncryptedData encryptedPin, std::string languageCode, int displayTimeout, int maximumTries, FreeText freeText, bool keepSession, std::function<void (const VerifyPinResult&)> resultCallback);

    ~VerifyPinFlow();
    


    /*!
     * \brief Set the free texts.
     * \param freeText A FreeText object containing 4 lines of text, exach with maximum 20 characters
     */
    VerifyPinFlow&& setFreeText(FreeText freeText);


    /*!
     * \brief Set the data of the message that will appear after the Success screen
     * \param displayTimeout The display timeout (of the screen)
     * \param firstTextIdentifier The identifier of the first text.
     * \param freeText A FreeText object containing 4 lines of text, exach with maximum 20 characters
     */
    VerifyPinFlow&& setSuccessNextMessageData(int displayTimeout, int firstTextIdentifier, boost::optional<FreeText> freeText);

    /*!
     * \brief Set the data of the message that will appear after the Fail screen
     * \param displayTimeout The display timeout (of the screen)
     * \param firstTextIdentifier The identifier of the first text.
     * \param freeText A FreeText object containing 4 lines of text, exach with maximum 20 characters
     */
    VerifyPinFlow&& setFailNextMessageData(int displayTimeout, int firstTextIdentifier, boost::optional<FreeText> freeText);


    std::string beginTransaction(TransactionContext&) override;
    bool endTransaction(const std::string& result, std::function<void(const std::string&)>responseCallback) override;
    std::unique_ptr<Transaction> move() override;


    VerifyPinFlow(VerifyPinFlow&&) = default;
    VerifyPinFlow& operator=(VerifyPinFlow&&) = default;

  private:
    VerifyPinFlow() = default;
    struct Impl;
    std::shared_ptr<Impl> impl_;
  };
  
}
